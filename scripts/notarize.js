const { notarize } = require('electron-notarize');

exports.default = async function notarizing(context) {
    const {electronPlatformName, appOutDir} = context;
    if (electronPlatformName !== 'darwin') {
        return;
    }

    const appName = context.packager.appInfo.productFilename;

    const notarizationResult = await notarize({
      appBundleId    : '8V8XY9T2G7.com.timerocket.spectre-kafka',
      appPath        : `${appOutDir}/${appName}.app`,
      appleId        : process.env.APPLE_USERNAME,
      appleIdPassword: process.env.APPLE_PASSWORD,
    });

    console.log(`Notarization result`, notarizationResult);

    return notarizationResult;
};