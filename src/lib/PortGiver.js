
class PortGiver {

    /**
     * @type {number}
     * @private
     */
    _currentPort;

    constructor(startingPort) {
        this._currentPort = startingPort;
    }

    getNextPort() {
        return this._currentPort++;
    }

}

module.exports = {PortGiver}