class EditorRow {
    source;
    source_field_types;

    constructor(source) {
        this.source = source;
        this.source_field_types = [];
        for (const prop in this.source) {
            this.source_field_types[prop] = typeof this.source[prop];
        }
    }

    getElement() {
        const elem = document.createElement("tr");
        elem.className = "editing";
        //elem.innerText = `EDIT ${JSON.stringify(this.source)}`;

        this.source_value_elements = [];

        for (const prop in this.source) {

            let edit_row_element = document.createElement("td");
            edit_row_element.innerText = this.source[prop];
            if (this.source_field_types[prop] === "boolean") {
                edit_row_element = document.createElement('select');
                edit_row_element.innerHTML = `<option value="1" ${!!this.source[prop] ? "selected":""}>true</option><option value="0" ${!this.source[prop] ? "selected":""}>false</option>`;
            }
            edit_row_element.className = "padded";
            if (prop !== "_id" && this.source_field_types[prop] !== "boolean") {
                edit_row_element.contentEditable = true;
            }
            this.source_value_elements[prop] = edit_row_element;
            elem.appendChild(edit_row_element);

        }


        const done_elem = document.createElement("td");
        done_elem.className = "clickable";
        done_elem.innerHTML = `<i class="fas fa-check-circle"></i>`;

        done_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;

            for (const prop in this.source) {

                switch (this.source_field_types[prop]) {
                    case "string":
                        this.source[prop] = this.source_value_elements[prop].innerText;
                        break;
                    case "number":
                        let parsed_string_number = this.source_value_elements[prop].innerText.match(/[-]{0,1}[\d]*[.]{0,1}[\d]+/g);
                        //console.log("A",parsed_string_number);
                        if (!parsed_string_number){
                            parsed_string_number = null;
                        }else{
                            parsed_string_number = parseFloat(parsed_string_number[0]);
                        }
                        this.source[prop] = parsed_string_number;
                        break;
                    case "boolean":
                        this.source[prop] = (this.source_value_elements[prop].value)==="1";
                        break;
                }
            }

            this.publish("done", this.rollupDocument(this.source));
        };


        const cancel_elem = document.createElement("td");
        cancel_elem.className = "clickable";
        cancel_elem.innerHTML = `<i class="fas fa-undo"></i>`;

        cancel_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;
            this.publish("cancel");
        };


        elem.appendChild(done_elem);
        elem.appendChild(cancel_elem);

        return elem;
    }

    // Thanks to https://stackoverflow.com/users/4620771/nenad-vracar
    rollupDocument() {
        var result = {}
        for (var i in this.source) {
            var keys = i.split('.')
            keys.reduce((r, e, j) => {
                return r[e] || (r[e] = isNaN(Number(keys[j + 1])) ? (keys.length - 1 == j ? this.source[i] : {}) : [])
            }, result)
        }
        return result
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }
}

module.exports = EditorRow;