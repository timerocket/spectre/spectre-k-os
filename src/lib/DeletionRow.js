class DeletionRow {
    source;
    driver;

    constructor(source, driver) {
        this.source = source;
        this.driver = driver;
    }

    getElement() {
        const elem     = document.createElement("tr");
        elem.className = "deleting";

        let action_container_td = document.createElement("td");
        action_container_td.className= "actions";
        action_container_td.colSpan = 1;
        let action_container_td_table = document.createElement("table");
        let action_container_td_table_tr = document.createElement("tr");
        //elem.innerText = `EDIT ${JSON.stringify(this.source)}`;

        this.source_value_elements = [];

        const done_elem     = document.createElement("span");
        done_elem.className = "clickable";
        done_elem.style.marginRight="0.5em";
        done_elem.innerHTML = `<i class="fas fa-trash-alt"></i>`;

        done_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;
            //   for (const prop in this.source) {
            //       this.source[prop] = this.source_value_elements[prop].innerText;
            //   }

            this.publish("done", this.rollupDocument(this.source));
        };


        const cancel_elem     = document.createElement("span");
        cancel_elem.className = "clickable";
        cancel_elem.style.marginLeft="0.5em";
        cancel_elem.innerHTML = `<i class="fas fa-undo"></i>`;

        cancel_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;
            this.publish("cancel");
        };

        let elem_actions_container = document.createElement("td");
        elem_actions_container.className = "padded";
        elem_actions_container.className = "clickable";
        elem_actions_container.style.whiteSpace = "pre";
        elem_actions_container.appendChild(done_elem);
        elem_actions_container.appendChild(cancel_elem)

        action_container_td_table_tr.appendChild(elem_actions_container);

        action_container_td_table.append(action_container_td_table_tr);
        action_container_td.append(action_container_td_table);
        elem.append(action_container_td);


        for (const prop in this.source) {

            const edit_row_element           = document.createElement("td");
            //edit_row_element.className = "clickable";
            edit_row_element.className       = "padded";
            //edit_row_element.contentEditable = true;
            edit_row_element.innerText       = this.source[prop];
          //  this.source_value_elements[prop] = edit_row_element;
            elem.appendChild(edit_row_element);

        }





        return elem;
    }

    // Thanks to https://stackoverflow.com/users/4620771/nenad-vracar
    rollupDocument() {
        var result = {}
        for (var i in this.source) {
            var keys = i.split('.')
            keys.reduce((r, e, j) => {
                return r[e] || (r[e] = isNaN(Number(keys[j + 1])) ? (keys.length - 1 == j ? this.source[i] : {}) : [])
            }, result)
        }
        return result
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }
}

module.exports = DeletionRow;