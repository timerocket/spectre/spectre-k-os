class EventSource {
    static get DISCONNECT() { return "DISCONNECT" }
    static get REFRESH() { return "REFRESH" }
    static get STARTUP() { return "STARTUP" }
}

module.exports = { EventSource }