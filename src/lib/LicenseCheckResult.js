
class LicenseCheckResult {

    /**
     * @type {LicenseCheckStatusType}
     */
    status;

    /**
     * @type {Date | undefined}
     */
    cancelledAt;

    /**
     * @type {Date | undefined}
     */
    failedAt;

    /**
     * @type {number}
     */
    uses;

    /**
     * @type {string}
     */
    errorMessage;

    /**
     * @type {Date}
     */
    lastVerified;

    constructor(status) {
        this.status = status;
    }
}

module.exports = {LicenseCheckResult}