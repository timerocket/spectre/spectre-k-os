
class SettingsDriver {

    static get DEFAULT_CACHE_SIZE() { return 1024 * 1024 * 1024 }

    warnRepublish = 1;

    constructor() {
        this.cacheSize = SettingsDriver.DEFAULT_CACHE_SIZE;
        this.warnRepublish = 1;
        this._initCacheSize();
        this._initWarnRepublish();
    }


    /**
     * @private
     */
    _initCacheSize() {
        let json_saved = window.localStorage.getItem('_spectre_kafka_gui_cache_size');
        if (json_saved) {
            this.cacheSize = parseInt(json_saved);
        }
    }

    _initWarnRepublish() {
        const warnRepublish = window.localStorage.getItem('_spectre_kafka_gui_warn_republish');
        if (warnRepublish) {
            this.warnRepublish = warnRepublish === "1" ? true : false;
        }
    }

    /**
     * @param {boolean} warn
     */
    setWarnRepublish(warn) {
        this.warnRepublish = warn;
        window.localStorage.setItem('_spectre_kafka_gui_warn_republish', (warn ? 1 : 0).toString());
    }

    toggleWarnRepublish() {
        this.setWarnRepublish(!this.warnRepublish);
    }

    getSettings(){
        return {
            cacheSize: this.cacheSize,
            warnRepublish: this.warnRepublish
        }
    }
}

module.exports = { SettingsDriver }