const https                = require('https')
const {LicenseCheckStatus} = require('./LicenseCheckStatus');
const {LicenseCheckResult} = require('./LicenseCheckResult');

const licenseVerificationOptions = {
    hostname: 'api.gumroad.com',
    port    : 443,
    path    : '/v2/licenses/verify',
    method  : 'POST',
    headers : {
        'Content-Type'  : 'application/json',
        'Content-Length': 0
    }
}

class LicenseKey {

    constructor() {
        this.user_key = window.localStorage.getItem("_spectre_k_gui_user_key");
        const lastVerified = window.localStorage.getItem("_spectre_k_gui_user_key_last_verified");
        if(lastVerified) {
            this.lastVerified = new Date(lastVerified);
        }
    }

    /**
     * @returns {Promise<LicenseCheckResult>}
     */
    async isAuthorized() {
        if (process.env.RUNTIME === "mas") {
            // TODO check apple subscription status
            return new LicenseCheckResult(LicenseCheckStatus.ACTIVE)
        }

        if (this.user_key && this.user_key.length === ((8 * 4) + 3)) {
            return await this.checkAuthorized(this.user_key);
        }

        return new LicenseCheckResult(LicenseCheckStatus.NO_LICENSE_KEY);
    }

    /**
     * @param {string} key
     * @returns {Promise<LicenseCheckResult>}
     */
    async checkAuthorized(key) {

        return new Promise((resolve) => {

            licenseVerificationOptions.path = `/v2/licenses/verify?product_permalink=spectre-kafka-gui&license_key=${key}&increment_uses_count=false`;
            let data                        = "";
            const req                       = https.request(licenseVerificationOptions, res => {
                // console.log(`statusCode: ${res.statusCode}`)
                res.on('data', d => {
                    if (res.statusCode === 200) {
                        data += d.toString();
                    }
                })

                res.on('end', error => {
                    if (data.length === 0) {
                        resolve(new LicenseCheckResult(LicenseCheckStatus.INVALID_LICENSE_KEY));
                        return;
                    }
                    let license_data = JSON.parse(data);

                    if (license_data.uses > license_data.purchase.quantity * 10 && !license_data.purchase.refunded) {
                        const result = new LicenseCheckResult(LicenseCheckStatus.TOO_MANY_USES);
                        result.uses = license_data.uses
                        resolve(result);
                        return;
                    }

                    if (license_data.subscription_ended_at && new Date(license_data.subscription_ended_at).getTime() <= new Date().getTime()) {
                        if(license_data.subscription_failed_at) {
                            const result = new LicenseCheckResult(LicenseCheckStatus.PAYMENT_FAILED);
                            result.failedAt = new Date(license_data.subscription_failed_at)
                            resolve(result);
                        }else{
                            const result = new LicenseCheckResult(LicenseCheckStatus.CANCELLED);
                            result.cancelledAt = new Date(license_data.subscription_cancelled_at)
                            resolve(result);
                        }
                        return;
                    }

                    this.lastVerified = new Date();
                    window.localStorage.setItem("_spectre_k_gui_user_key_last_verified", this.lastVerified.toISOString());
                    resolve(new LicenseCheckResult(LicenseCheckStatus.ACTIVE));
                })
            })

            req.on('error', error => {
                const result = new LicenseCheckResult(LicenseCheckStatus.UNABLE_TO_VERIFY);
                result.errorMessage = error.message;
                if(!this.lastVerified){
                    resolve(result);
                    return;
                }
                // Allow user 1 week of use without being able to verify
                const maxTimestampWithoutVerification = this.lastVerified.getTime() + (7 * 24 * 60 * 60 * 1000)
                if(new Date().getTime() > maxTimestampWithoutVerification) {
                    result.lastVerified = this.lastVerified;
                    resolve(result);
                }else{
                    result.status = LicenseCheckStatus.SKIPPED_VERIFICATION;
                    result.lastVerified = this.lastVerified;
                }
                resolve(result);
            })

            req.end();

        });

    }

    revokeKey() {
        this.user_key = null;
        window.localStorage.removeItem("_spectre_k_gui_user_key");
    }

    consumeKey(key) {

        return new Promise((resolve) => {

            licenseVerificationOptions.path = `/v2/licenses/verify?product_permalink=spectre-kafka-gui&license_key=${key}&increment_uses_count=true`;
            const req                       = https.request(licenseVerificationOptions, (res) => {
                this.user_key = key;
                window.localStorage.setItem("_spectre_k_gui_user_key", key);
                resolve(true);
            })

            req.on('error', error => {
                resolve(false);
            })

            req.end();

        });
    }

}

module.exports = LicenseKey;