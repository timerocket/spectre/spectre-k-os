const ejs                      = require("ejs");
const fs                       = require("fs");
const filePath                 = require("./FilePath");
const flatObject               = require("./FlatObject");
const EditorRow                = require("./EditorRow");
const DeletionRow              = require("./DeletionRow");
const TypeFromProperty         = require("./TypeFromProperty");
const {EmptyTableStateMachine} = require('./empty-table/EmptyTableStateMachine');
const {EmptyTableState}        = require('./empty-table/EmptyTableState');

class KafkaSmartTable {

    static get TYPE_TOPIC() {
        return "TOPIC"
    }

    static get TYPE_MESSAGE() {
        return "MESSAGE"
    }

    table_container_element;
    app;
    elements_per_page   = 10;
    page_number         = 0;
    total_elements      = 0;
    table_element;
    fixed_data_source;
    available_actions;
    collection_topic;
    headers             = [];
    query;
    pagination_element;
    table_rows_selector_element;
    filter_field;
    match_type;
    subscribers         = [];
    cached_filters      = [];
    show_value_selector = false;
    show_value_columns  = false;
    set_data            = false;
    isPaused            = false;

    /**
     * @private
     * @type {EmptyTableStateMachine}
     */
    _emptyTableStateMachine;

    /**
     * @private
     */
    _tableType;

    get empty_table_message() {
        return this._emptyTableStateMachine.state.message;
    }

    /**
     * @param {Router} app
     * @param {*[]} fixed_data_source
     * @param {*[]} available_actions
     * @param {string} topic
     * @param {"TOPIC" | "MESSAGE" } type
     */
    constructor(app, fixed_data_source, available_actions = [], topic, type) {
        if (fixed_data_source) {
            this.updateFixedDataSource(fixed_data_source);
        }
        this.collection_topic = topic;

        this.available_actions       = available_actions;
        this.app                     = app;
        this.table_container_element = document.createElement('div');
        this.match_type              = "fuzzy";
        this._emptyTableStateMachine = new EmptyTableStateMachine();
        this._tableType              = type;
    }

    /**
     * @param {"PAUSED" | "AWAITING" | "LOADING_TOPICS }state
     */
    setEmptyStateType(state) {
        this._emptyTableStateMachine.setStateType(state);
    }

    setStateForStateType(state, type) {
        this._emptyTableStateMachine.setStateForStateType(state, type);
    }

    getLoadingTopicsState() {
        return this._emptyTableStateMachine.getLoadingTopicsState();
    }

    getNoTopicsState() {
        return this._emptyTableStateMachine.getNoTopicsState();
    }

    /**
     * @returns {EmptyTableState}
     */
    getEmptyTableState() {
        return this._emptyTableStateMachine.state;
    }

    updateFixedDataSource(fixed_data_source) {
        if(!fixed_data_source) {
            return;
        }
        this.fixed_data_source = fixed_data_source;
        this.total_elements    = fixed_data_source.length;
        this.set_data          = true;
    }

    async render(element) {

        try {
            this.table_container_element.innerHTML =
                ejs.render(fs.readFileSync(filePath("../view/smart-table.ejs")).toString(), {
                    match_type         : this.match_type,
                    show_value_columns : this.show_value_columns,
                    show_value_selector: this.show_value_selector
                }, null);
            element.appendChild(this.table_container_element);

            this.table_element = document.getElementById("smart-table-element");

            const search_input = document.getElementById("smart-table-filter");

            this.pagination_element          = document.getElementById("smart-table-pagination");
            this.table_rows_selector_element = document.getElementById("smart-table-row-selector");

            const toggle_show_values_as_columns = document.getElementById("show_value_columns");
            if (toggle_show_values_as_columns) {
                toggle_show_values_as_columns.onclick = () => {
                    this.show_value_columns = !this.show_value_columns;
                    this.publish("refresh");
                }
            }

            if (this.query) {
                search_input.value = this.query;
            }

            this.cached_filters = null;

            let refresh_btn = document.getElementById("smart-table-refresh-rows");

            refresh_btn.onclick = () => {
                this.publish("refresh",{startSpinner: true, stopSpinnerOnCompletion:true});
            }

            this.match_selector = document.getElementById("smart-table-exact-match");

            this.match_selector.onchange = async () => {
                this.match_type = this.match_selector.value;
                await this.clearTable();
                await this.updateTable();
            };

            search_input.onkeyup = async () => {

                this.query = search_input.value;
                await this.clearTable();
                await this.updateTable();
            }

            await this.updateTable();
        } catch (e) {
            console.error(e);
        }

    }

    async clearTable() {
        this.table_element.innerHTML = "";
    }

    async updateTable() {
        let data = await this.queryData();

        if (!document.getElementById("smart-table-message")) {
            return;
        }

        if(data.length === 0){
            if (this._tableType === KafkaSmartTable.TYPE_TOPIC) {
                if (!this.query) {
                    this._emptyTableStateMachine.setStateType(EmptyTableState.NO_TOPICS);
                } else {
                    this._emptyTableStateMachine.setStateType(EmptyTableState.NO_MATCHING_TOPICS);
                }
            }
            if (this._tableType === KafkaSmartTable.TYPE_MESSAGE) {
                if (!this.query) {
                    if (this.isPaused) {
                        this._emptyTableStateMachine.setStateType(EmptyTableState.PAUSED);
                    } else {
                        this._emptyTableStateMachine.setStateType(EmptyTableState.AWAITING);
                    }
                } else {
                    this._emptyTableStateMachine.setStateType(EmptyTableState.NO_MATCHING_MESSAGES);
                }
            }

            document.getElementById("smart-table-message").innerHTML = ejs.render(fs.readFileSync(filePath("../view/smart-table-waiting-results.ejs")).toString(), {
                message   : this.empty_table_message,
                inProgress: this._emptyTableStateMachine.state.inProgress
            }, null)
            this.updateFilterField();
            await this.updatePagination();

            if(this.set_data && this.fixed_data_source) {
                return;
            }
        }else{
            document.getElementById("smart-table-message").innerHTML = '';
        }

        const headers = await this.updateHeadersFromRows(data);
        const rows    = await this.renderRows(data);

        this.updateFilterField();
        await this.updatePagination();

    }

    updateFilterField() {

        const select_element = document.getElementById("smart-table-filter-select")

        if (!this.cached_filters || this.cached_filters.length !== ["*", ...this.headers].length) {
            this.cached_filters      = ["*", ...this.headers];
            select_element.innerHTML = "";

            for (let sel of this.cached_filters) {
                const option     = document.createElement("option");
                option.innerText = this.getColumnNameFix(sel);
                option.value = (sel);

                if (sel === this.filter_field) {
                    option.selected = true;
                }

                select_element.appendChild(option);
            }

            if (this.cached_filters.length <= 1){
                document.getElementById("smart-table-filter-field").style.display = "none";
            }else{
                document.getElementById("smart-table-filter-field").style.display = "block";
            }

            select_element.onchange = async (e) => {
                this.filter_field = select_element.value;
                await this.clearTable();
                await this.updateTable();
            }
        }

    }

    async updatePagination() {

        let row_selection = [10, 25, 50, 100, 250, 500];

        if (this.table_rows_selector_element.innerHTML === "") {
            this.table_rows_selector_element.innerHTML = `<div class="flex-margin-right" style="width: max-content;"><small>Rows per page:</small><select id="smart-table-select-rows"></select></div>`;

            const select_element = document.getElementById("smart-table-select-rows");

            for (let sel of row_selection) {

                const option     = document.createElement("option");
                option.innerText = sel;

                if (sel === this.elements_per_page) {
                    option.selected = true;
                }

                select_element.appendChild(option);
            }

            select_element.onchange = async (e) => {
                this.elements_per_page = parseInt(select_element.value);
                await this.clearTable();
                await this.updateTable();
            }
        }

        let pagination_container = document.getElementById("smart-table-pagination-container");
        if (!pagination_container) {
            pagination_container           = document.createElement("div");
            pagination_container.className = "pagination noselect";
            pagination_container.id        = "smart-table-pagination-container";
            this.pagination_element.appendChild(pagination_container);
        }

        let display_page_number = this.page_number + 1;

        let pages = (((this.total_elements - 1) / this.elements_per_page) | 0) + 1;

        display_page_number = Math.min(display_page_number, pages);


        let pagecount = document.getElementById("smart-table-page-count");

        if (!pagecount) {
            pagecount    = document.createElement("small");
            pagecount.id = "smart-table-page-count";
            pagination_container.appendChild(pagecount);
        }
        pagecount.innerHTML = `Page <span class="highlight">${display_page_number}</span> of <span class="highlight">${pages}</span>`;

        let prev = document.getElementById("page-prev");//ocument.createElement("button");
        if (!prev) {
            prev           = document.createElement("button");
            prev.rel       = "prev";
            prev.innerText = "Previous";
            prev.id        = "page-prev";//`<button ${display_page_number <= 1 ? 'disabled' : ''} rel="prev">Previous</button>`;
            pagination_container.appendChild(prev);
        }
        prev.disabled  = (display_page_number <= 1);



        let page_links = document.getElementById("smart-table-page-links");
        if (!page_links) {
            page_links = document.createElement("span");
            page_links.id= "smart-table-page-links";
            page_links.style.padding = "0 4px"
            pagination_container.appendChild(page_links);
        }
        page_links.innerHTML = "";
        for (let i = Math.max(display_page_number - 2, 1); i <= Math.min(display_page_number + 2, pages); i++) {
            let page_link_elem = document.createElement('div');
            page_link_elem.id  = `jump-page-${i}`;
            page_link_elem.className=`page_item${display_page_number === i ? ' active' : ''}`;
            page_link_elem.innerText = i;
            page_links.appendChild(page_link_elem);
        }

        let next = document.getElementById("page-next");
        if (!next) {
            next                   = document.createElement("button");
            next.rel               = "next";
            next.innerText         = "Next";
            next.style.marginRight = "8px";
            next.id                = "page-next";
            pagination_container.appendChild(next);
        }
        next.disabled = (display_page_number > pages - 1);

        for (let i = Math.max(display_page_number - 2, 1); i <= Math.min(display_page_number + 2, pages); i++) {

            if (i !== display_page_number) {
                const jump_page = document.getElementById(`jump-page-${i}`);
                if (jump_page) {
                    jump_page.onclick = async () => {
                        this.page_number = i - 1;
                        await this.clearTable();
                        await this.updateTable();
                    }
                }
            }
        }

        if (display_page_number > 1) {
            let prev_element = document.getElementById("page-prev");
            if (prev_element) {
                prev_element.onclick = async () => {
                    this.page_number--;
                    await this.clearTable();
                    await this.updateTable();
                }
            }

        }
        if (display_page_number < pages) {
            let next_element = document.getElementById("page-next");
            if (next_element) {
                next_element.onclick = async () => {
                    this.page_number++;
                    await this.clearTable();
                    await this.updateTable();
                }
            }

        }

    }

    getIconForHeader(prop, type) {
        prop           = prop.toLowerCase();
        let numb_check = parseFloat(type);
        if (numb_check !== "NaN" && numb_check.toString() === type) {
            type = "number";
        } else {
            type = typeof type;
        }
        if (prop[0] === "_") {
            return "";
        }
        if (type === "number") {
            return `<i class="fas fa-hashtag"></i>&nbsp;`;
        }
        if (type === "boolean") {
            return `<i class="fas fa-toggle-on"></i>&nbsp;`;
        }
        if (prop === "created" || prop === "modified" || prop === "accessed" || prop === "registered") {
            return `<i class="far fa-calendar-alt"></i>&nbsp;`;
        }
        if (type === "string") {
            return `<i class="fas fa-align-left"></i>&nbsp;`;
        }
        return `<i class="fas fa-question-circle"></i>&nbsp;`;
    }

    getColumnNameFix(column){
        return column.replace("_value.","");
    }


    async updateHeadersFromRows(data) {

        let header_row = document.getElementById("smart-table-header");
        if (!header_row) {
            header_row    = document.createElement("tr");
            header_row.id = "smart-table-header";
            this.table_element.prepend(header_row);
        }
        let flat = {};
        data.forEach((item) => {
            flat = Object.assign(flat, flatObject(item));
            for (let prop in flat) {
                if (this.headers.indexOf(prop) === -1) {
                    this.headers.push(prop);
                }
            }
        });

        if (this.available_actions.length) {
            const id = `header-actions`;
            if (!document.getElementById(id)) {

                const action_element_header = document.createElement("th");
                action_element_header.id = id;
                action_element_header.className = "actions";
                action_element_header.colSpan = this.available_actions.length;
                action_element_header.innerHTML = "<small>Row Actions</small>";
                header_row.appendChild(action_element_header);
            }
        }

        let sorters = [];


        let show_headers = this.getHeaders();

        for (let prop of show_headers) {

            const id = `header-${prop}`;
            if (!document.getElementById(id)) {
                const header_element     = document.createElement("th");
                header_element.id        = id;
                header_element.innerHTML = this.getIconForHeader(prop, flat[prop]) + this.getColumnNameFix(prop);

                const sorter     = document.createElement("span");
                sorter.className = "sorter";
                sorter.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;
                if (this.sort) {
                    if (this.sort[prop]) {
                        if (this.sort[prop].order === "desc") {
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort-down"></i>`;
                        } else {
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort-up"></i>`;
                        }
                    }
                }
                header_element.appendChild(sorter);

                sorters.push(sorter);

                sorter.onclick = async () => {

                    if (!this.sort || !this.sort[prop]) {
                        this.sort       = {};
                        this.sort[prop] = {order: "desc", unmapped_type: "long"};
                        for (const _s of sorters) {
                            _s.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;
                        }
                        sorter.innerHTML = `<i class="fas fa-fw fa-sort-down"></i>`;
                    } else {
                        if (this.sort[prop].order === "desc") {
                            this.sort[prop]  = {order: "asc", unmapped_type: "long"};
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort-up"></i>`;
                        } else if (this.sort[prop].order === "asc") {
                            this.sort        = null;
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;
                        }
                    }

                    let d = await this.queryData();
                    if (d.length === 0) {

                        sorter.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;

                        if (this.parsing_error.indexOf("Text fields are not optimised for operations that require per-document field data") !== -1) {
                            this.parsing_error = `Property [${prop}] was not correctly set up to be sorted on, this generally happens with text fields.<br /><br/>For more information about sorting and mapping, refer to the following documentation:<br /><a target="_blank" href="https://www.elastic.co/guide/en/elasticsearch/reference/current/sort-search-results.html#sort-search-results">https://www.elastic.co/guide/en/elasticsearch/reference/current/sort-search-results.html</a><br/><small>${this.parsing_error}</small>`;
                        }

                        let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/modal-notify.ejs")).toString(), {
                            data : `<p class="error">${this.parsing_error}</p>`,
                            title: "Sorting Error"
                        }, null), {class: "wide"});

                        let close_element     = document.getElementById('close');
                        this.sort             = null;
                        close_element.onclick = () => {
                            modal.close();
                        }

                    } else {
                        this.publish("refresh", {})
                    }
                }
                header_row.appendChild(header_element);

            }
        }

    }

    getHeaders() {
        let show_headers = [].concat(this.headers);
        if (this.show_value_selector) {
            if (this.show_value_columns) {
                let idx = show_headers.indexOf("value");
                show_headers.splice(idx, 1);
            } else {
                show_headers = [];
                for (const prop of this.headers) {
                    if (prop[0] !== "_") {
                        show_headers.push(prop);
                    }
                }
            }
        }
        return show_headers;
    }

    async renderRows(data) {

        for (const doc of data) {
            const id = `row-${doc._id || doc.index || doc.topic || doc.name || (doc.timestamp) || "error"}`;
            if (document.getElementById(id)) {
                continue;
            }
            const row_element     = document.createElement("tr");
            row_element.className = "zebra";
            if (this.fixed_data_source) {
                row_element.className += " clickable";
            }
            let show_headers = [].concat(this.headers);
            let show_item    = Object.assign({}, doc);

            if (this.show_value_selector) {
                if (this.show_value_columns) {
                    let idx = show_headers.indexOf("value");
                    show_headers.splice(idx, 1);
                    delete show_item.value;
                } else {
                    show_headers = [];
                    for (const prop of this.headers) {
                        if (prop[0] !== "_") {
                            show_headers.push(prop);
                        }
                    }
                }
            }

            row_element.innerHTML = ejs.render(fs.readFileSync(filePath("../view/smart-table-row.ejs")).toString(), {
                headers: show_headers,
                item   : show_item
            }, null);
            row_element.id        = id;
            row_element.onclick   = async () => {
                await this.publish("select", doc);
            }

            let action_elements = await this.attachActions(row_element, doc);
            if (action_elements.length) {
                let action_container_td          = document.createElement("td");
                action_container_td.className    = "actions";
                action_container_td.colSpan      = action_elements.length;
                let action_container_td_table    = document.createElement("table");
                let action_container_td_table_tr = document.createElement("tr");

                for (const action of action_elements) {
                    action_container_td_table_tr.appendChild(action);
                }

                action_container_td_table.appendChild(action_container_td_table_tr);
                action_container_td.appendChild(action_container_td_table);
                row_element.prepend(action_container_td)
            }

            this.table_element.appendChild(row_element);
        }

    }

    async attachActions(row_element, doc) {
        let elements = [];
        for (const key of this.available_actions) {
            switch (key) {

                case "delete":
                    const delete_row_element     = document.createElement("td");
                    delete_row_element.className = "clickable";
                    delete_row_element.innerHTML = `<i class="fas fa-trash-alt"></i>`;

                    delete_row_element.onclick = async (e) => {
                        e.preventDefault();
                        e.cancelBubble = true;

                        row_element.style.display = "none";

                        const deletion_row      = new DeletionRow(doc, this.app.connection.getKafkaDriver());
                        const deletion_row_elem = deletion_row.getElement();

                        row_element.parentNode.insertBefore(deletion_row_elem, row_element.nextSibling);

                        deletion_row.subscribe("done", async (updated) => {
                            row_element.style.display = "table-row";
                            deletion_row_elem.parentNode.removeChild(deletion_row_elem);
                            try {

                                let consumer = this.app.connection.getKafkaDriver().consumers[updated.name];

                                if (consumer) {
                                    await consumer.stop();
                                    await consumer.disconnect();
                                }

                                await this.app.connection.getKafkaDriver().deleteTopic(updated.name);

                                row_element.parentNode.removeChild(row_element);

                                let idx = -1;
                                for (let i = 0; i < this.fixed_data_source.length; i++) {
                                    let item = this.fixed_data_source[i];
                                    if (item.name === updated.name) {
                                        idx = i;
                                        break;
                                    }
                                }

                                if (idx !== -1) {
                                    this.fixed_data_source.splice(idx, 1);
                                }

                            } catch (e) {
                                console.log('ERROR IN DELETION', e);

                                let message = e.message;
                                if(e.code && e.code === 42){
                                    message += '<br/><br/>This can also happen when cluster setting `delete.topic.enable` is set to false'
                                }
                                // LOL https://github.com/tulios/kafkajs/issues/1106
                                message = message.replaceAll("sen't", "sent");

                                let m = new Modal(`<div class='modal-error'><p class="error"><strong>Topic Deletion Error:</strong><br/>${message}</p></div><br /><div class="flex">
    <button id="modal-error-close" style="width:100%;"><i class="fas fa-times"></i> Close</button>
</div>`);

                                document.getElementById("modal-error-close").onclick = () => {
                                    m.close();
                                }
                            }
                        });

                        deletion_row.subscribe("cancel", async () => {
                            row_element.style.display = "table-row";
                            deletion_row_elem.parentNode.removeChild(deletion_row_elem);
                        });

                    };

                    elements.push(delete_row_element);
                    break;

                case "republish":
                    const republish_row_element     = document.createElement("td");
                    republish_row_element.className = "clickable";
                    republish_row_element.innerHTML = `<i class="fas fa-redo"></i>`;

                    republish_row_element.onclick = async (e) => {
                        e.preventDefault();
                        e.cancelBubble = true;


                        let settings = (await this.app.publish('get-settings'))[0];

                        let repub = async () => {
                            const modal                        = new Modal(`<i class="fas fa-4x fa-spin fa-circle-notch"></i><h4>Sending Message...</h4>`);
                            modal.modal_window.style.textAlign = "center";
                            modal.modal_window.style.color     = "#c7d7ef";

                            try {
                                await this.app.connection.getKafkaDriver().producer.send({
                                    topic   : this.collection_topic.name,
                                    messages: [
                                        {
                                            key  : doc.key ? doc.key : null,
                                            value: doc.value ? doc.value : null
                                        }
                                    ]
                                });
                                modal.close();
                            } catch (e) {

                                modal.modal_window.innerHTML = `<div class='modal-error'>${e.message}</div><br /><div class="flex">
    <button id="cancel" style="width:100%;"><i class="fas fa-times"></i> Close</button>
</div>`;

                                let close_element     = document.getElementById('cancel');
                                close_element.onclick = () => {
                                    modal.close();
                                }

                            }
                        }

                        if (settings.warnRepublish) {

                            let modal             = new Modal(ejs.render(fs.readFileSync(filePath("../view/republish.ejs")).toString(), {}, null), {class: "wide"});
                            let close_element     = document.getElementById('close');
                            close_element.onclick = () => {
                                modal.close();
                            }

                            let send_element     = document.getElementById('send');
                            send_element.onclick = () => {
                                let warn = document.getElementById('warn_republish_check').value;
                                //console.log( document.getElementById('warn_republish_check'),warn);
                                if (warn === "on") {
                                    this.app.publish('spectre_kafka_gui_warn_republish', 0);
                                }
                                modal.close();
                                repub();
                            }

                        } else {
                            await repub();
                        }

                    }

                    elements.push(republish_row_element);
                    break;

                case "edit":
                    const edit_row_element     = document.createElement("td");
                    edit_row_element.className = "clickable";
                    edit_row_element.innerHTML = `<i class="fas fa-edit"></i>`;

                    edit_row_element.onclick = async (e) => {
                        e.preventDefault();
                        e.cancelBubble = true;

                        row_element.style.display = "none";

                        const editor_row      = new EditorRow(doc);
                        const editor_row_elem = editor_row.getElement();

                        row_element.parentNode.insertBefore(editor_row_elem, row_element.nextSibling);

                        editor_row.subscribe("done", async (updated) => {
                            row_element.style.display = "table-row";
                            editor_row_elem.parentNode.removeChild(editor_row_elem);
                            row_element.innerHTML = ejs.render(fs.readFileSync(filePath("../view/smart-table-row.ejs")).toString(), {
                                item   : flatObject(updated),
                                headers: this.getHeaders()
                            }, null);
                            this.attachActions(row_element, doc);
                            await this.publish("edit", updated);
                            await this.updateTable();
                        });

                        editor_row.subscribe("cancel", async () => {
                            row_element.style.display = "table-row";
                            editor_row_elem.parentNode.removeChild(editor_row_elem);
                        });

                    };

                    elements.push(edit_row_element);
                    break;

            }
        }
        return elements;
    }

    async queryData() {
        if (this.fixed_data_source) {
            let filtered = this.fixed_data_source.filter((item) => {
                if (!this.query || this.query === "") {
                    return true;
                }
                let match = false;

                for (let prop in item) {
                    if (this.match_type==="fuzzy") {
                        if (item[prop].toString().toLowerCase().indexOf(this.query.toLowerCase()) !== -1 && ((!this.filter_field || this.filter_field === "*") || (this.filter_field === prop))) {
                            match = true;
                        }
                    }else{
                        if (((!this.filter_field || this.filter_field === "*") || (this.filter_field === prop)) && (item[prop].toString() === this.query)) {
                            match = true;
                        }
                    }
                }


                return match;
            });

            if (this.sort) {
                for (const prop in this.sort) {
                    filtered = filtered.sort((a, b) => {

                        let field_type = TypeFromProperty(a[prop]);

                        if (this.sort[prop].order === "desc") {
                            if (field_type === "string") {
                                if (a[prop] < b[prop]) {
                                    return -1;
                                }
                                if (a[prop] > b[prop]) {
                                    return 1;
                                }
                                return 0;
                            } else if (field_type === "number") {
                                return (parseFloat(a[prop]) - parseFloat(b[prop]))
                            } else {
                                return a[prop] - b[prop];
                            }
                        } else {
                            if (field_type === "string") {
                                if (b[prop] < a[prop]) {
                                    return -1;
                                }
                                if (b[prop] > a[prop]) {
                                    return 1;
                                }
                                return 0;
                            } else if (field_type === "number") {
                                return (parseFloat(b[prop]) - parseFloat(a[prop]))
                            } else {
                                return b[prop] - a[prop];
                            }
                        }
                    });
                }
            }

            let page_number     = Math.min(this.page_number, (filtered.length / this.elements_per_page) | 0);
            this.total_elements = filtered.length;
            return filtered.slice(page_number * this.elements_per_page, (page_number + 1) * this.elements_per_page);
        }

    }

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }

}

module.exports = KafkaSmartTable;
