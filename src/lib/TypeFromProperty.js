function TypeFromProperty(prop) {
    let parsed_string_number = (prop || "undefined").toString().match(/[-]{0,1}[\d]*[.]{0,1}[\d]+/g);
    if (parsed_string_number || !isNaN(parseFloat(prop))) {
        return "number";
    }
    if (prop === true || prop === false || prop === "true" || prop === "false") {
        return "boolean";
    }

    return "string";

}

module.exports = TypeFromProperty;