const {Connection}     = require('./connection/Connection');
const {SettingsDriver} = require('./SettingsDriver');

class Router {

    /**
     * @type {Connection}
     */
    connection;

    /**
     * @private
     */
    _controller;

    subscribers = [];

    constructor() {
        this.settings   = new SettingsDriver();
        this.connection = new Connection(this, this.settings);
    }

    async route(controller, source = null) {
        if (this._controller) {
            await this._controller.blur();
        }
        this._controller = controller;
        await controller.render();
        controller.focus(source);
        controller.publish("focus");
    }

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }
        let results = [];
        this.subscribers[idx].forEach(async (fn) => {
            results.push(await fn(data));
        });
        return results;
    }
}

module.exports = Router;