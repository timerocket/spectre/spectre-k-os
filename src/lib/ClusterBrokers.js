const evilDns = require('evil-dns');
const net     = require("net");
const dns     = require('dns');

class ClusterBrokers {

    /**
     * @private {string[]}
     */
    _brokerHosts;

    /**
     * @private {string}
     */
    _targetIp;

    /**
     * @private {ConnectionDetails}
     */
    _config;

    /**
     * @private {string[]}
     */
    _uris;

    /**
     * @param {ConnectionDetails} config
     */
    constructor(config) {
        this._brokerHosts = [];
        this._uris        = [];
        this._config      = config;
    }

    async init() {

        if (this._config.auth_type.includes("-ssh") || this._config.auth_type === "k8s") {
            const uriParts = this._config.connectUri.split(':');
            const host     = uriParts[0];
            this._targetIp = host;
        } else {
            this._targetIp = await this._dnsLookup(this._config.inputUri);
        }

        for (const brokerUri of this._config.clusterBrokerUris) {
            this.add(brokerUri);
        }
    }

    _dnsLookup(uri) {
        const uriParts = uri.split(':');
        const host     = uriParts[0];
        return new Promise((resolve, reject) => {
            dns.lookup(host, (err, address, family) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(address);
                }
            });
        })
    }

    add(uri) {
        const uriParts = uri.split(':');
        let host       = uriParts[0];

        if (!this._brokerHosts.includes(host)) {
            if (!net.isIP(host)) {
                try {
                    evilDns.add(host, this._targetIp);
                }catch (e){
                    console.warn(e);
                }
            }
            this._brokerHosts.push(host);
        }

        if (!this._uris.includes(uri)) {
            this._uris.push(uri);
        }
    }

    getUris() {
        return this._uris;
    }
}

module.exports = {ClusterBrokers}