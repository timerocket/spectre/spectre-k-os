/**
 * @typedef { "PAYMENT_FAILED" | "ACTIVE" | "CANCELLED" | "NO_LICENSE_KEY" | "TOO_MANY_USES" | "INVALID_LICENSE_KEY" | "SKIPPED_VERIFICATION" } LicenseCheckStatusType
 */

class LicenseCheckStatus {
    static get PAYMENT_FAILED() {
        return "PAYMENT_FAILED"
    }

    static get ACTIVE() {
        return "ACTIVE"
    }

    static get SKIPPED_VERIFICATION() {
        return "SKIPPED_VERIFICATION"
    }

    static get CANCELLED() {
        return "CANCELLED"
    }

    static get NO_LICENSE_KEY() {
        return "NO_LICENSE_KEY"
    }

    static get INVALID_LICENSE_KEY() {
        return "INVALID_LICENSE_KEY"
    }

    static get TOO_MANY_USES() {
        return "TOO_MANY_USES"
    }

    static get UNABLE_TO_VERIFY() {
        return "UNABLE_TO_VERIFY"
    }

}

module.exports = {LicenseCheckStatus}