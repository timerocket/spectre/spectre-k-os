class Modal {
    element;
    modal_window;
    constructor(innerHTML) {
        this.element = document.createElement("div");
        this.element.className="flex modal-container";
        this.element.id="modal-window-id";

        this.modal_window = document.createElement("div");
        this.modal_window.className = "modal-window";
        this.modal_window.innerHTML = innerHTML;

        this.element.appendChild(this.modal_window);

        document.body.appendChild(this.element);
    }

    close(){
        if (this.element.parentNode && this.element) {
            this.element.parentNode.removeChild(this.element);
        }
    }
}

module.exports = Modal;