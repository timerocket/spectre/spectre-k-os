const ejs = require("ejs");
const fs = require("fs");
const filePath = require("../FilePath");
const os = require("os");

class SettingsController {

    app;
    stream_controller;
    messages_total = 0;

    /**
     * @type {SettingsDriver}
     */
    settingsDriver;

    /**
     * @param {Router} app
     */
    constructor(app) {
        this.subscribers = [];
        this.app = app;
        this.settingsDriver = app.settings;

        this.app.publish("update-settings-memory-cache-size", (app.settings.cacheSize).toFixed(2));

        app.subscribe("spectre_kafka_gui_warn_republish", (v)=>{
            this.settingsDriver.setWarnRepublish(v ? true : false);
        });

        app.subscribe("get-settings", ()=>{
            return this.app.settings.getSettings();
        });
    }

    async render() {

        const kafkaDriver = this.app.connection.getKafkaDriver();

        const tab_items = [{
            text: `<i class="far fa-caret-square-left"></i> Back`,
            id: "back"
        }
        ];

        let cache_used = this.app.connection.getKafkaDriver().getCacheUsed();

        console.log('CACHE SIZE ON RENDER', this.app.settings.cacheSize);

        console.log('REDNERING SETTINGS', this.messages_total, this.getMessagesTotals(),(this.app.settings.cacheSize / (1024 * 1024 * 1024)).toFixed(2));

        document.body.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/tabs.ejs")).toString(), {items: tab_items}, null)
            +
            ejs.render(fs.readFileSync(filePath("../view/settings.ejs")).toString(), {
                cache_used: (cache_used / (1024 * 1024 * 1024)).toFixed(4),
                settings_memory_cache_size: (this.app.settings.cacheSize / (1024 * 1024 * 1024)).toFixed(2),
                messages_count: this.messages_total,
                messages_current: this.getMessagesTotals(),
                settings_max_memory: os.totalmem(),
                ...this.app.settings.getSettings()
            }, null);
        return null;
    }

    setStreamController(c) {
        this.stream_controller = c;
    }

    getMessagesTotals() {
        let messages_in_memory = 0;
        if (this.app.connection && this.app.connection.getKafkaDriver()) {
            for (const prop in this.app.connection.getKafkaDriver().messages) {
                this.app.connection.getKafkaDriver().messages[prop].forEach((v) => {
                    messages_in_memory++;
                });
            }
        }
        return messages_in_memory;
    }

    async focus() {

        let settings_memory_cache_size_range = document.getElementById("settings-memory-cache-size");

        settings_memory_cache_size_range.onchange = settings_memory_cache_size_range.oninput = (e) => {
            this.app.settings.cacheSize = settings_memory_cache_size_range.value * (1024 * 1024 * 1024);
            window.localStorage.setItem('_spectre_kafka_gui_cache_size', this.app.settings.cacheSize);
            console.log("UPDATE FORM UI",settings_memory_cache_size_range.value,this.app.settings.cacheSize);
            this.app.publish("update-settings-memory-cache-size", (this.app.settings.cacheSize).toFixed(2));


            let update_size_elems = document.getElementsByClassName('settings_memory_cache_size');
            for (const elem of update_size_elems) {
                elem.innerText = `${(this.app.settings.cacheSize / (1024 * 1024 * 1024)).toFixed(2)}`
            }

        }

        let settings_btn = document.getElementById('back');
        settings_btn.onclick = async () => {
            this.app.publish('settings-return');
        }
        let warn_check = document.getElementById('warn_republish');
        warn_check.onchange = async () => {
            this.settingsDriver.toggleWarnRepublish();
        }


        return null;
    }

    async blur() {
        return null;
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }
}

module.exports = SettingsController;