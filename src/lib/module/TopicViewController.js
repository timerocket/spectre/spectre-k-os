const ejs                = require("ejs");
const fs                 = require("fs");
const filePath           = require("../FilePath");
const KafkaSmartTable    = require("../KafkaSmartTable");
const Modal              = require("../Modal");
const flatObject         = require("../FlatObject");
const {EmptyTableState}  = require("../empty-table/EmptyTableState");
const {ConnectionStatus} = require("../connection/ConnectionStatus");
const {KafkaDriver}      = require('../connection/KafkaDriver');

class TopicViewController {

    app;
    smart_table;

    /**
     * @type {TopicData[]}
     * @private
     */
    _topics;

    /**
     * @type {number}
     * @private
     */
    _initialRefreshIntervalId;

    constructor(app) {
        this.subscribers = [];
        this.app         = app;
        this._topics     = [];
    }

    /**
     * @param {Router} app
     * @private
     */
    _initSubscriptions(app) {
        app.subscribe('connection.disconnections-complete', async () => {
            await this._refresh();
        });
    }

    async render() {

        let _topics       = this._topics;
        const kafkaDriver = this.app.connection.getKafkaDriver();
        if (kafkaDriver.getConnectionStatus() === ConnectionStatus.CONNECTED) {
            try {
                const getTopics = await kafkaDriver.getTopics();
                if (getTopics.length > 0) {
                    _topics      = getTopics;
                    this._topics = _topics;
                }
            } catch (e) {
                if (!KafkaDriver.isDisconnectedError(e)) {
                    console.error(e);
                } else {
                    kafkaDriver.setConnectionStatus(ConnectionStatus.DISCONNECTED);
                }
            }
        }

        const topics = _topics.map((topic) => {
            return flatObject(topic);
        });

        console.log("LOAD TOPICS", topics);

        const tab_items = [
            {
                text: `<i class="far fa-window-close"></i> Disconnect`,
                id  : "disconnect"
            },
            {
                text : `<i class="fas fa-cog"></i> Settings`,
                id   : "settings",
                class: "right"
            }, {
                text : `<i class="far fa-plus-square"></i> Create Topic`,
                id   : "new-topic",
                class: "right connected-only"
            }
        ];

        console.log("rendering topic view", this.app.connection.getKafkaDriver().getConnectionConfig());

        document.body.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/tabs.ejs")).toString(), {items: tab_items}, null) +
            ejs.render(fs.readFileSync(filePath("../view/topics.ejs")).toString(), {
                inputUri: this.app.connection.getKafkaDriver().getConnectionNickname() || this.app.connection.getKafkaDriver().inputUri,
                topics  : topics
            }, null);

        let smart_table_handlers = !!this.smart_table;
        this.smart_table         = this.smart_table || new KafkaSmartTable(this.app, topics, ['delete'], undefined, KafkaSmartTable.TYPE_TOPIC);
        if (topics.length === 0) {
            let fetchTopicsTryCount = 0;
            this.smart_table.setStateForStateType(this.smart_table.getLoadingTopicsState(), EmptyTableState.NO_TOPICS);
            this._initialRefreshIntervalId = setInterval(async () => {
                if (kafkaDriver.getConnectionStatus() === ConnectionStatus.CONNECTED) {
                    fetchTopicsTryCount++;
                    if (fetchTopicsTryCount > 5) {
                        clearInterval(this._initialRefreshIntervalId);
                        this.smart_table.setStateForStateType(this.smart_table.getNoTopicsState(), EmptyTableState.NO_TOPICS);
                        this.smart_table.render(document.getElementById("topic-table"));
                        return;
                    }
                    const theTopics = await this.app.connection.getKafkaDriver().getTopics();
                    if (theTopics.length > 0) {
                        clearInterval(this._initialRefreshIntervalId);
                        this.smart_table.updateFixedDataSource(theTopics);
                        this.smart_table.render(document.getElementById("topic-table"));
                        this.smart_table.setStateForStateType(this.smart_table.getNoTopicsState(), EmptyTableState.NO_TOPICS);
                    }
                }
            }, 200);
        }
        this.smart_table.setEmptyStateType(EmptyTableState.LOADING_TOPICS);

        this.smart_table.updateFixedDataSource(topics);

        this.smart_table.render(document.getElementById("topic-table"));

        if (!smart_table_handlers) {
            this.smart_table.subscribe("select", (index) => {
                this.publish("select-topic", index);
            });
        }

        this.smart_table.subscribe("refresh", async () => {
            await this._refresh();
        });

        let settings_btn     = document.getElementById('settings');
        settings_btn.onclick = async () => {
            this.app.publish('settings', this);
        }

        document.getElementById("new-topic").onclick = () => {
            this.createNewTopicModal();
        }

        document.getElementById("disconnect").onclick = () => {
            clearInterval(this._initialRefreshIntervalId);
            this._topics = [];
            this.publish("disconnect");
        }

        return null;
    }

    async _refresh() {
        let refresh_btn        = document.getElementById("smart-table-refresh-rows");
        let refresh_icon       = document.getElementById("refresh-icon");
        refresh_icon.className = "fas fa-spin fa-sync";
        refresh_btn.disabled   = "disabled";
        const topics           = await this.app.connection.getKafkaDriver().getTopics();
        this.smart_table.updateFixedDataSource(topics.map((topic) => {
            return flatObject(topic);
        }));
        await this.smart_table.clearTable();
        await this.smart_table.updateTable();
        refresh_btn.disabled   = false;
        refresh_icon.className = "fas fa-sync";
    }

    createNewTopicModal() {
        let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/new-topic.ejs")).toString(), {}, null));

        let topic_element      = document.getElementById('topic');
        let partitions_element = document.getElementById('partitions');
        let replicas_element   = document.getElementById('replicationFactor');
        let create_element     = document.getElementById('create');
        let cancel_element     = document.getElementById('cancel');

        topic_element.onclick = () => {
            topic_element.style.backgroundColor = "#60728b";
        }

        create_element.onclick = async () => {

            if (!topic_element.value) {
                topic_element.style.backgroundColor = `#e25353`;
                return false;
            }

            modal.modal_window.style.color                            = "#c7d7ef";
            document.getElementById('new-topic-form').style.display   = "none";
            document.getElementById('new-topic-status').innerHTML     = `<i class="fas fa-4x fa-spin fa-circle-notch"></i><h4>Creating Topic...</h4>`;
            document.getElementById('new-topic-status').style.display = "block";


            let exists = false;
            try {
                let topics = await this.app.connection.getKafkaDriver().getTopics();

                topics.forEach((topic) => {
                    if (topic.name === topic_element.value) {
                        exists = true;
                    }
                });
            }catch (e){
                document.getElementById('new-topic-form').style.display      = "block";
                document.getElementById('new-topic-status').style.display    = "none";
                document.getElementById("new-topic-error-message").innerHTML = `<p class="error">${e.message}</p>`;
            }


            if (!exists) {
                let result = false;
                let err    = false;

                try {
                    const options = {
                        topics: [
                            {
                                topic            : topic_element.value,
                                numPartitions    : parseInt(partitions_element.value) || 1,
                                replicationFactor: parseInt(replicas_element.value) || 1
                            }]
                    }

                    result = await new Promise((resolve, reject) => {
                        this.app.connection.getKafkaDriver().createTopics(options)
                            .then(r => {
                                resolve(r);
                            })
                            .catch(e => {
                                reject(e);
                            });
                        setTimeout(() => {
                            if(!result && !err){
                                reject(new Error('Timed out'));
                            }
                        }, 10000)
                    })

                } catch (e) {
                    err = e;
                }

                if (!result) {
                    const new_topic_form = document.getElementById('new-topic-form');
                    if(new_topic_form){
                        document.getElementById('new-topic-form').style.display      = "block";
                        document.getElementById('new-topic-status').style.display    = "none";
                        document.getElementById("new-topic-error-message").innerHTML = `<p class="error">${err.message}</p>`;
                    }
                } else {
                    modal.close();
                    await this.render();
                    this.publish("focus");
                }
            } else {
                modal.close();
            }


        }

        cancel_element.onclick = () => {
            modal.close();
        }
    }

    async focus() {
        return null;
    }

    async blur() {
        return null;
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }


}

module.exports = TopicViewController;