const ejs                           = require("ejs");
const fs                            = require("fs");
const flatObject                    = require("../FlatObject");
const filePath                      = require("../FilePath");
const KafkaSmartTable               = require("../KafkaSmartTable");
const Modal                         = require("../Modal");
const {MessageProducerCrashHandler} = require("../error-handler/MessageProducerCrashHandler");
const {ConsumerCrashHandler}        = require('../error-handler/ConsumerCrashHandler');
const {ConnectionStatus}            = require('../connection/ConnectionStatus');
const {KafkaDriver}                 = require('../connection/KafkaDriver');

class TopicStreamController {

    app;
    topic;
    smart_table = [];
    subscribers = [];
    producers   = [];

    paused                  = [];
    cacheSize               = 1024 * 1024 * 1024;
    unread_messages         = [];
    messages_as_json        = [];
    consuming_from_offset_0;
    pause_clicked_timestamp = Number.MAX_SAFE_INTEGER;

    offset_element;

    /**
     * @type {ConsumerCrashHandler}
     * @private
     */
    _consumerCrashHandler;

    /**
     * @type {boolean}
     * @private
     */
    _wasNotConnected;

    _lastKafkaDriver;

    _refreshDebounceTimeout;
    _refreshDebounceLast;

    _refreshUIDebounceTimeout;
    _refreshUIDebounceLast;

    constructor(app) {
        this.app                     = app;
        this.consuming_from_offset_0 = false;

        app.subscribe("update-settings-memory-cache-size", (v) => {
            this.cacheSize = v;
        });

        window.setInterval(() => {
            this.app.publish("settings-garbage-collect", 1);
        }, 10000)

        this._consumerCrashHandler = new ConsumerCrashHandler();
        this._wasNotConnected      = false;
        this._refreshDebounceLast  = Date.now();
        this._refreshUIDebounceLast  = Date.now();
        this._initSubscriptions();
        this._initLowOffsetInterval();
    }

    _initLowOffsetInterval() {
        setInterval(async () => {
            const offset_element = document.getElementById("offset");
            const kafkaDriver    = this._lastKafkaDriver;
            if (offset_element && kafkaDriver.getConnectionStatus() === ConnectionStatus.CONNECTED) {
                offset_element.innerHTML = await this._getOffsetElementInnerHtml(kafkaDriver);
            }
        }, 5000);
    }

    setHeaderPauseBtn() {
        let btn = document.getElementById("pause");
        if (!btn) {
            return
        }
        btn.className = "tab-item right";
        if (this.paused[this.topic.name] === undefined) {
            this.paused[this.topic.name] = false;
        }
        if (this.paused[this.topic.name]) {
            btn.innerHTML = `<i class="fas fa-play-circle"></i> Resume Messages<div id="message-count-pill" class="pill"></div>`;
        } else {
            btn.innerHTML = `<i class="fas fa-pause-circle"></i> Pause Messages`;
        }
    }

    async _getOffsetElementInnerHtml(kafkaDriver) {
        let offsetLow = 0;
        if (kafkaDriver.getConnectionStatus() === ConnectionStatus.CONNECTED) {
            offsetLow = await kafkaDriver.getTopicOffsetLow(this.topic.name);
        }

        return `<i class="fas fa-download"></i> Load from Offset ${offsetLow}`;
    }

    async render() {

        const kafkaDriver     = this.app.connection.getKafkaDriver();
        this._lastKafkaDriver = kafkaDriver;

        const tab_items = [
            {
                text: `<i class="far fa-caret-square-left"></i> View All Topics`,
                id  : "back"
            }, {
                text : `<i class="fas fa-cog"></i> Settings`,
                id   : "settings",
                class: "right"
            }, {
                text : `<i class="fas fa-times"></i> Clear All Messages`,
                id   : "clear",
                class: "right"
            }, {
                text : await this._getOffsetElementInnerHtml(kafkaDriver),
                id   : "offset",
                class: "right connected-only"
            }, {
                text : `<i class="fas fa-pause-circle"></i> Pause Messages`,
                id   : "pause",
                class: "right"
            }, {
                text : `<i class="fas fa-sign-in-alt"></i> Publish Message`,
                id   : "produce",
                class: "right connected-only"
            }
        ];

        console.log('RENDERING STREAM CONTROLLER TOPIC', this.topic);

        document.body.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/tabs.ejs")).toString(), {items: tab_items}, null)
            + ejs.render(fs.readFileSync(filePath("../view/elements.ejs")).toString(), {
                topic: this.topic
            }, null);


        this.setHeaderPauseBtn();

        this.offset_element = document.getElementById("offset");

        await this._initConsumer(kafkaDriver);

        let smart_table_handlers = true;
        if (!this.smart_table[this.topic.name]) {
            smart_table_handlers                                  = false;
            this.smart_table[this.topic.name]                     = new KafkaSmartTable(this.app, kafkaDriver.messages[this.topic.name], ["republish"], this.topic, KafkaSmartTable.TYPE_MESSAGE);
            this.smart_table[this.topic.name].show_value_selector = true;
        }

        await this.smart_table[this.topic.name].render(document.getElementById("elements-table"));

        this.smart_table[this.topic.name].subscribe("refresh-start", () => {
            let refresh_btn  = document.getElementById("smart-table-refresh-rows");
            let refresh_icon = document.getElementById("refresh-icon");
            if (refresh_icon && refresh_btn) {
                refresh_icon.className = "fas fa-spin fa-sync";
                refresh_btn.disabled   = "disabled";
            }
        });

        this.smart_table[this.topic.name].subscribe("refresh-complete", () => {
            let table_refresh = document.getElementById("smart-table-refresh-rows");
            let refresh_icon  = document.getElementById("refresh-icon");

            if (table_refresh && refresh_icon) {
                table_refresh.disabled = false;
                refresh_icon.className = "fas fa-sync";
            }
            this.offset_element.className = this.offset_element.className.replace(" disabled", "");
        });

        const refreshData = async () => {
            if (!this.topic) {
                return;
            }
            this.unread_messages[this.topic.name] = 0;
            this.setHeaderPauseBtn();
            this.smart_table[this.topic.name].updateFixedDataSource(kafkaDriver.messages[this.topic.name]);
            await this.smart_table[this.topic.name].clearTable();
            await this.smart_table[this.topic.name].updateTable();
        }

        if (!smart_table_handlers) {
            this.smart_table[this.topic.name].subscribe("select", (doc) => {
                this.publish("select-row", doc);
            });

            this.smart_table[this.topic.name].subscribe("refresh", async (data) => {
                if (!data) {
                    data = {}
                }
                if (this._refreshDebounceTimeout) {
                    if (this._refreshDebounceLast < (Date.now()) - (100)) {
                        return;
                    } else {
                        clearTimeout(this._refreshDebounceTimeout);
                    }
                }
                const topic                  = this.topic;
                this._refreshDebounceTimeout = window.setTimeout(async () => {

                    if (!data || data.startSpinner) {
                        this.smart_table[topic.name].publish("refresh-start");
                    }
                    await refreshData();
                    if (!data || data.stopSpinnerOnCompletion) {
                        setTimeout(() => {
                            this.smart_table[topic.name].publish("refresh-complete");
                        }, 25);
                    }
                    this._refreshDebounceTimeout = null;
                    this._refreshDebounceLast    = Date.now();
                }, 25);

            });
        }


        let settings_btn     = document.getElementById('settings');
        settings_btn.onclick = async () => {
            this.app.publish('settings', this);
        }

        document.getElementById("back").onclick = () => {
            this.topic = null;
            this.publish("back")
        }

        const togglePause                        = () => {
            let btn = document.getElementById("pause");
            if (btn.className.indexOf("disabled") !== -1) {
                return;
            }
            this.paused[this.topic.name] = !this.paused[this.topic.name];
            this.setHeaderPauseBtn();
            if (this.paused[this.topic.name]) {
                this.smart_table[this.topic.name].isPaused = true;
            } else {
                this.unread_messages[this.topic.name]      = 0;
                this.smart_table[this.topic.name].isPaused = false;
            }

        }
        document.getElementById("pause").onclick = () => {
            togglePause();
            this.pause_clicked_timestamp = new Date().getTime();
            this.smart_table[this.topic.name].publish("refresh", {startSpinner: false});
        }

        const clearAllMessages = async () => {
            this.app.publish("messages-clear", kafkaDriver.messages[this.topic.name].length);
            kafkaDriver.messages[this.topic.name] = [];
            this.smart_table[this.topic.name].updateFixedDataSource(kafkaDriver.messages[this.topic.name]);
            await this.smart_table[this.topic.name].clearTable();
            await this.smart_table[this.topic.name].updateTable();
        }

        document.getElementById("clear").onclick = async () => {
            await clearAllMessages();
        }

        this.offset_element.onclick = async () => {
            if (this.offset_element.className.indexOf("disabled") !== -1) {
                return;
            }
            this.consuming_from_offset_0          = true;
            kafkaDriver.messages[this.topic.name] = kafkaDriver.messages[this.topic.name].filter((msg) => {
                return msg.timestamp < this.pause_clicked_timestamp;
            });

            this.app.publish("messages-clear", kafkaDriver.messages[this.topic.name].length);
            this.unread_messages[this.topic.name] = 0;
            kafkaDriver.messages[this.topic.name] = [];

            this.smart_table[this.topic.name].publish("refresh", {startSpinner: true, stopSpinnerOnCompletion: true});

            const offsetLow = await kafkaDriver.getTopicOffsetLow(this.topic.name);
            console.log(`LOAD FROM OFFSET ${offsetLow}`);
            this.offset_element.className += " disabled";
            const offsetHigh = await kafkaDriver.getTopicOffsetHigh(this.topic.name);


            if (offsetHigh === offsetLow) {
                this.smart_table[this.topic.name].publish("refresh", {
                    startSpinner           : false,
                    stopSpinnerOnCompletion: true
                });
                await new Promise(resolve => {
                    setTimeout(resolve, 250)
                });
                this.offset_element.className = this.offset_element.className.replace(" disabled", "");
                return;
            }

            kafkaDriver.consumers[this.topic.name].seek({topic: this.topic.name, partition: 0, offset: offsetLow})
        }

        document.getElementById("produce").onclick = () => {


            if (this.messages_as_json[this.topic.name] === undefined) {
                this.messages_as_json[this.topic.name] = true;
            }

            let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/produce-message.ejs")).toString(), {
                topic           : this.topic,
                messages_as_json: this.messages_as_json[this.topic.name]
            }, null));

            let key_element     = document.getElementById('key');
            let message_element = document.getElementById('message');
            let insert_element  = document.getElementById('insert');
            let cancel_element  = document.getElementById('cancel');

            message_element.onclick = () => {
                message_element.style.backgroundColor = "#60728b";
            }

            insert_element.onclick = async () => {
                let valid_json = false;
                let insert_json_data;
                try {
                    insert_json_data = JSON.parse(message_element.value);
                    console.log(insert_json_data);
                    valid_json = true;
                } catch (e) {

                }

                let check_valid                        = document.getElementById("produce-validate-json").checked;
                this.messages_as_json[this.topic.name] = check_valid;


                if ((check_valid) && (!message_element.value || !valid_json)) {
                    message_element.style.backgroundColor = `#e25353`;
                    return false;
                }

                modal.modal_window.style.textAlign = "center";
                modal.modal_window.style.color     = "#c7d7ef";
                modal.modal_window.innerHTML       = `<i class="fas fa-4x fa-spin fa-circle-notch"></i><h4>Sending Message...</h4>`;

                try {
                    await kafkaDriver.producer.send({
                        topic   : this.topic.name,
                        messages: [
                            {
                                key  : key_element.value ? key_element.value : null,
                                value: message_element.value
                            }
                        ]
                    });
                    modal.close();
                } catch (e) {
                    modal.close();
                    console.log('SENDING ERRRO', e);
                    let producer_messager_error_handler = new MessageProducerCrashHandler();
                    producer_messager_error_handler.handle(e);

                }


            }

            cancel_element.onclick = () => {
                let check_valid                        = document.getElementById("produce-validate-json").checked;
                this.messages_as_json[this.topic.name] = check_valid;
                modal.close();
            }

        }

        return null;
    }

    async _initConsumer(kafkaDriver) {
        if (!this.topic) {
            return;
        }
        if (!kafkaDriver.messages[this.topic.name]) {
            kafkaDriver.messages[this.topic.name]  = [];
            kafkaDriver.topicAges[this.topic.name] = 0;
        }

        if (kafkaDriver.getConnectionStatus() === ConnectionStatus.CONNECTED && !kafkaDriver.consumers[this.topic.name]) {
            try {
                kafkaDriver.consumers[this.topic.name] = await kafkaDriver.consumerClient.consumer(
                    {
                        "groupId": kafkaDriver.consumerGroupId + "_" + this.topic.name,
                        retry    : {
                            restartOnFailure: async () => {
                                return kafkaDriver.shouldRestart()
                            },
                            retries         : 0,
                        },
                    });
                kafkaDriver.consumers[this.topic.name].on('consumer.crash', (e) => {
                    this._consumerCrashHandler.handle(e);
                    kafkaDriver.onConsumerCrash(e).then();
                })
                await kafkaDriver.consumers[this.topic.name].connect();
                kafkaDriver.consumers[this.topic.name].subscribe({topic: this.topic.name});
                kafkaDriver.consumers[this.topic.name].run(
                    {
                        eachMessage: async ({topic, partition, message}) => {

                            try {
                                if (!kafkaDriver.messages[topic]) {
                                    console.log("NOT LISTENING TO THIS TOPIC", topic);
                                    return;
                                }

                                let msg = {
                                    timestamp: parseInt(message.timestamp),
                                    key      : (message.key || "").toString(),
                                    value    : (message.value || "").toString()
                                };

                                let message_exists = false;
                                /* kafkaDriver.messages[topic].forEach((m) => {
                                      if (m.timestamp === msg.timestamp && m.value === msg.value) {
                                          message_exists = true;
                                      }
                                  })*/

                                if (msg.timestamp > this.pause_clicked_timestamp) {
                                    this.consuming_from_offset_0 = false;
                                }

                                if (!message_exists) {
                                    if (!msg.data) {
                                        try {
                                            msg = Object.assign(msg, flatObject({_value: JSON.parse(msg.value)}));
                                        } catch (e) {

                                        }
                                    }

                                    if (this.paused[topic] && !this.consuming_from_offset_0) {
                                        if (!this.unread_messages[topic]) {
                                            this.unread_messages[topic] = 0;
                                        }
                                        this.unread_messages[topic]++;
                                    }

                                    kafkaDriver.messages[topic].unshift(msg);
                                    this.smart_table[topic].updateFixedDataSource(kafkaDriver.messages[topic]);
                                    this.app.publish("message-new");
                                    if (this.topic && this.topic.name === topic) {
                                        if (this.offset_element.className.indexOf("disabled") !== -1) {
                                            this.offset_element.className = this.offset_element.className.replace(" disabled", "");
                                        }

                                        this.updateUIFromNewMessage(topic);
                                        if (!this.paused[topic] || this.consuming_from_offset_0) {
                                            this.smart_table[topic].publish("refresh", {
                                                startSpinner           : false,
                                                stopSpinnerOnCompletion: false
                                            });
                                        }
                                    }
                                    if (this.topic) {
                                        kafkaDriver.topicAges[topic] = Date.now();
                                    }

                                }
                            } catch (e) {
                                console.log(e);
                            }

                            if (this.topic && this.smart_table[this.topic.name]) {
                                this.smart_table[this.topic.name].publish("refresh-complete");
                            }
                        },
                    }
                );
            } catch (e) {
                if (!KafkaDriver.isDisconnectedError(e)) {
                    console.error(e);
                } else {
                    kafkaDriver.setConnectionStatus(ConnectionStatus.DISCONNECTED);
                }
            }
        }
    }

    updateUIFromNewMessage(topic) {

        if (this._refreshUIDebounceTimeout) {
            if (this._refreshUIDebounceLast < (Date.now()) - (100)) {
                return;
            } else {
                clearTimeout(this._refreshUIDebounceTimeout);
            }
        }
        this._refreshUIDebounceTimeout = window.setTimeout(async () => {

            if (this.paused[topic]) {
                if (this.unread_messages[topic] > 0) {
                    document.getElementById("message-count-pill").innerHTML = `<span>${this.unread_messages[topic]}</span>`;
                } else {
                    document.getElementById("message-count-pill").innerHTML = '';
                }
            }

            let btn = document.getElementById("pause");

            if (btn && btn.className.indexOf("disabled") !== -1) {
                this.setHeaderPauseBtn();
            }

        }, 25);
    }

    _initSubscriptions() {
        this.app.subscribe('connection.connected', async () => {
            if (this._wasNotConnected) {
                await this._initConsumer(this.app.connection.getKafkaDriver());
                this._enableUnavailableActions();
            }
            this._wasNotConnected = false;

        })
        this.app.subscribe('connection.lost', () => {
            this._disableUnavailableActions();
            this._wasNotConnected = true;
        });
        this.app.subscribe('connection.retry-started', () => {
            this._disableUnavailableActions();
            this._wasNotConnected = true;
        });
    }

    _enableUnavailableActions() {
        if (document.body.classList.contains('not-connected')) {
            document.body.classList.remove('not-connected');
        }
    }

    _disableUnavailableActions() {
        if (!document.body.classList.contains('not-connected')) {
            document.body.classList.add('not-connected');
        }
    }

    async focus() {
        if (!this.topic.name) {
            await this.smart_table[this.topic.name].updateTable();
        }
        return null;
    }

    async blur() {

        return null;
    }

    setTopic(index) {
        this.topic = index;
    }

    subscribe(index, fn) {
        if (!this.subscribers[index]) {
            this.subscribers[index] = [];
        }
        this.subscribers[index].push(fn);
    }

    async publish(index, data) {
        if (!this.subscribers[index]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[index]) {
                await fn(data);
            }
            resolve();
        });

    }

}

module.exports = TopicStreamController;