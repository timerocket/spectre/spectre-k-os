const ejs                 = require("ejs");
const fs                  = require("fs");
const filePath            = require("../FilePath");
const {ConnectionResult}  = require("../connection/ConnectionResult");
const {ConnectionDetails} = require("../connection/ConnectionDetails");
const {EventSource}       = require("../EventSource");
const {Connection}        = require('../connection/Connection');
const Modal               = require('../Modal');

class ConnectController {

    app;

    /**
     * @type {ConnectionDetails}
     */
    use_saved;

    /**
     * @type {boolean}
     */
    didInit;

    /**
     * @type {Connection}
     */
    connection;

    constructor(app) {
        this.subscribers     = [];
        this.connection      = app.connection;
        this.app             = app;
        this.enableDomEvents = true;
        this.didInit         = false;
    }

    async render() {

        const tab_items = [
            {
                text : `<i class="fas fa-cog"></i> Settings`,
                id   : "settings",
                class: "right no-border"
            }
        ];


        document.body.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/tabs.ejs")).toString(), {style: "", items: tab_items}, null)
            + ejs.render(fs.readFileSync(filePath("../view/connect.ejs")).toString(), Object.assign(this.connection.getConnectionDetails(), this.use_saved), null);
        return null;
    }


    async attemptConnection(config) {
        let modal                                 = new Modal(`<h1><i class="fas fa-fw fa-spin fa-spinner"></i> Connecting...</h1><br /><div class="flex">
    <button id="cancel" style="width:100%;"><i class="fas fa-times"></i> Cancel</button>
</div>`);
        document.getElementById('cancel').onclick = () => {
            this.connection.disconnect();
            modal.close();
        };
        try {
            let connect = await this.connection.connect(config);
            modal.close();
            return connect;
        }catch (e){
            modal.close();
            throw e;
        }
    }

    /**
     * @param { string } source
     * @returns {Promise<null>}
     */
    async focus(source) {

        if (source !== EventSource.DISCONNECT) {
            const inputUri         = document.getElementById("uri").value;
            const connectionConfig = this.getConnectionConfig(inputUri);
            if (!connectionConfig.shouldPrompt()) {
                //  await this.checkURIStatus();
            }
        }

        const uriInput   = document.getElementById("uri");
        uriInput.onkeyup = uriInput.onchange = uriInput.onclick = async () => {
            if (!this.enableDomEvents) {
                return;
            }

            this.publish("uri-reset").then();
            this.publish('check-result-reset').then();
        }

        let settings_btn     = document.getElementById('settings');
        settings_btn.onclick = async () => {
            this.app.publish('settings', this);
        }

        let connect                  = document.getElementById('connect');
        let save_connection_checkbox = document.getElementById("save_connection");
        let save_connection_nickname = document.getElementById("save_connection_nickname");

        save_connection_checkbox.onchange = () => {
            save_connection_nickname.style.display = (save_connection_checkbox.checked) ? "inline-block" : "none";
        }


        let connection_auth_type = document.getElementById("connection_auth_type");

        const k8s_service_input   = document.getElementById("k8s_service");
        const k8s_namespace_input = document.getElementById("k8s_namespace");
        const k8s_port_input      = document.getElementById("k8s_port");
        const updateK8sUri        = () => {
            uriInput.value = `${k8s_service_input.value}.${k8s_namespace_input.value}.svc.cluster.local:${k8s_port_input.value}`
        }

        k8s_service_input.onchange   = updateK8sUri;
        k8s_namespace_input.onchange = updateK8sUri;
        k8s_port_input.onchange      = updateK8sUri;

        connection_auth_type.onchange = (e) => {
            if (!this.enableDomEvents) {
                return;
            }
            let types = document.getElementsByClassName("connection_type");
            console.log("TYPES", types);
            for (let elem of types) {
                elem.style.display = "none";
            }
            if (connection_auth_type.value.includes("-ssh")) {
                document.getElementById("ssh").style.display = "block";
            } else {
                document.getElementById(connection_auth_type.value).style.display = "block";
            }
            if (connection_auth_type.value === "k8s") {
                uriInput.disabled = true;
            } else {
                uriInput.disabled = false;
            }
        }

        connect.onclick = async () => {
            this.enableDomEvents = false;
            try {
                let checked_save = save_connection_checkbox.checked;

                await this.publish('check-result-spinner');
                const inputUri      = document.getElementById("uri").value;
                const connectConfig = this.getConnectionConfig(inputUri);
                const message       = await this.attemptConnection(connectConfig);

                console.log('RESULT', message, message.result, ConnectionResult.SUCCESS, message.result !== ConnectionResult.SUCCESS);

                if (message.result === ConnectionResult.SUCCESS) {
                    this.publish('uri-reset').then();
                    this.publish('connect').then();
                    if (checked_save) {
                        this.connection.addRecentConnection(connectConfig);
                    }
                    this.app.publish("connected");
                }

                if ([ConnectionResult.TIMEOUT, ConnectionResult.UNKNOWN_FAILURE].includes(message.result)) {
                    this.publish('check-result-bad', message.message).then();
                    this.publish('uri-bad').then();
                }

                if ([ConnectionResult.SKIPPED, ConnectionResult.CANCELED].includes(message.result)) {
                    return;
                }

            } catch (e) {
                console.error(e);
                this.publish('check-result-bad', e.message).then();
                this.publish('uri-bad').then();
            }
            this.enableDomEvents = true;
        }

        let recent = this.connection.getRecentConnections();

        if (recent && recent.length) {
            for (let i = 0; i < recent.length; i++) {
                let el     = document.getElementById("recent-connection-" + i);
                el.onclick = async () => {
                    this.enableDomEvents = false;
                    this.use_saved       = recent[i];
                    await this.publish("check-result-spinner");
                    await this.publish("refresh");

                    try {
                        const message = await this.attemptConnection(this.use_saved);
                        if (message.result === ConnectionResult.SUCCESS) {
                            window.localStorage.setItem("_spectre_k_gui_last-connection_index", i.toString());
                            this.publish('connect').then();
                        }

                        if ([ConnectionResult.TIMEOUT, ConnectionResult.UNKNOWN_FAILURE].includes(message.result)) {
                            this.publish("uri-bad").then();
                            this.publish('check-result-bad', message.message).then();
                        }

                        if ([ConnectionResult.SKIPPED, ConnectionResult.CANCELED].includes(message.result)) {
                            return;
                        }

                    } catch (e) {
                        this.enableDomEvents = true;
                        throw e;
                    }
                    this.enableDomEvents = true;
                }

                document.getElementById("remove-recent-connection-" + i).onclick = () => {
                    if (!this.enableDomEvents) {
                        return;
                    }
                    if (window.localStorage.getItem("_spectre_k_gui_last-connection_index") === i.toString()) {
                        window.localStorage.setItem("_spectre_k_gui_last-connection_index", "-1");
                    }
                    this.connection.getRecentConnections().splice(i, 1);
                    this.connection.saveRecentConnections();

                    document.getElementById("recent-connection-handle-" + i).style.display = "none";
                }
            }
        }

        if (!this.didInit) {
            const lastConnectionIndex = window.localStorage.getItem("_spectre_k_gui_last-connection_index");
            if (lastConnectionIndex !== undefined && parseInt(lastConnectionIndex, 10) >= 0) {
                const index    = parseInt(lastConnectionIndex, 10);
                let recent     = this.connection.getRecentConnections();
                this.use_saved = recent[index];
                await this.publish("refresh");
            }
            this.didInit = true;
        }


        return null;
    }

    async checkURIStatus() {

        this.publish('check-result-spinner').then();
        this.publish('uri-reset').then();

        const inputUri         = document.getElementById("uri").value;
        const connectionConfig = this.getConnectionConfig(inputUri);

        try {
            const connectionResultMessage = await this.connection.testConnection(connectionConfig);

            if (connectionResultMessage.result === ConnectionResult.SUCCESS) {
                this.publish('check-result-good').then();
            } else {
                this.publish('check-result-bad', connectionResultMessage.message).then();
            }
        } catch (e) {
            this.publish('check-result-bad', e.message).then();
        }
    }

    getConnectionConfig(inputUri) {
        return new ConnectionDetails(
            {
                username          : document.getElementById("username").value.trim(),
                password          : document.getElementById("password").value,
                sasl_mechanism    : document.getElementById("sasl_mechanism").value.trim(),
                ssl_enabled       : document.getElementById("ssl_enabled").checked,
                ssl_self_signed   : document.getElementById("ssl_self_signed").checked,
                confluent_username: document.getElementById("confluent_username").value.trim(),
                confluent_password: document.getElementById("confluent_password").value,
                nickname          : document.getElementById("save_connection_nickname").value.trim(),
                ssh_path          : this._getFile("ssh_path"),
                ssh_username      : document.getElementById("ssh_username").value.trim(),
                ssh_password      : document.getElementById("ssh_password").value.trim(),
                ssh_passphrase    : document.getElementById("ssh_passphrase").value.trim(),
                ssh_host          : document.getElementById("ssh_host").value.trim(),
                ssh_port          : document.getElementById("ssh_port").value.trim(),
                ssh_kafka_username: document.getElementById("ssh_kafka_username").value.trim(),
                ssh_kafka_password: document.getElementById("ssh_kafka_password").value.trim(),
                ssh_use_rsa       : document.getElementById("ssh_use_rsa").checked,
                k8s_context       : document.getElementById("k8s_context").value.trim(),
                k8s_service       : document.getElementById("k8s_service").value.trim(),
                k8s_port          : document.getElementById("k8s_port").value.trim(),
                k8s_namespace     : document.getElementById("k8s_namespace").value.trim(),
                k8s_username      : document.getElementById("k8s_username").value.trim(),
                k8s_password      : document.getElementById("k8s_password").value,
                k8s_sasl_mechanism: document.getElementById("k8s_sasl_mechanism").value.trim(),
                k8s_ssl_enabled   : document.getElementById("k8s_ssl_enabled").checked,
                aiven_user        : document.getElementById("aiven_user").value.trim(),
                aiven_password    : document.getElementById("aiven_password").value.trim(),
                aiven_ca_path     : this._getFile("aiven_ca_path"),
                aiven_key_path    : this._getFile("aiven_key_path"),
                aiven_cert_path   : this._getFile("aiven_cert_path"),
                auth_type         : document.getElementById("connection_auth_type").value.trim(),
                inputUri
            }
        )
    }

    _getFile(name) {
        const file_element  = document.getElementById(name);
        const input_element = document.getElementById(`${name}_text`);
        if (input_element.value) {
            return input_element.value.trim();
        }
        if (file_element.files && file_element.files[0] && file_element.files[0].path) {
            return file_element.files[0].path
        }
        return "";
    }

    async blur() {
        return null;
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }
}

module.exports = ConnectController;