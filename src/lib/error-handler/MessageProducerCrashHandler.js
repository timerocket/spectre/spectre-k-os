const Modal = require("../Modal");

class MessageProducerCrashHandler {

    handle(e) {
        console.info("Producer crashed", e);

        if (this._shouldShowError(e)) {
            this.renderModal(e);
        }
    }

    _shouldShowError(e) {

        if (this._isRetryable(e)) {
            return false;
        }

        return true;
    }



    _isRetryable(e) {
        return e && e.error && e.error.retriable;
    }


    renderModal(e) {
        if (document.getElementById("modal-producer-message-crash-error")) {
            return;
        }

        let modal = new Modal(`<div class='modal-error-message' id='modal-producer-message-crash-error'><p class="error">${e.message}</p></div><br /><div class="flex">
    <button id="modal-error-crash-close" style="width:100%;"><i class="fas fa-times"></i> Close</button>
</div>`);

        document.getElementById("modal-error-crash-close").onclick = () => {
            modal.close();
        }
    }

}

module.exports = {MessageProducerCrashHandler}