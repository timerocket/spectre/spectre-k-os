const Modal = require("../Modal");

class ConsumerCrashHandler {

    handle(e) {
        console.info("Consumer crashed", e);

        if (this._shouldShowError(e)) {
            this.renderModal(e);
        }
    }

    _shouldShowError(e) {
        if (this._isBrokerNotConnectedError(e)) {
            return false;
        }
        if (this._isLeaderNotAvailableError(e)) {
            return false;
        }
        if (this._isConnectionTimeoutError(e)) {
            return false;
        }
        if (this._isBrokerNotFoundError(e)) {
            return false;
        }
        if (this._isRetryable(e)) {
            return false;
        }
        if(this._isEConnRefusedError(e)){
            return false;
        }
        if(this._isGroupCoordinatorNotFoundError(e)) {
            return false;
        }
        if(this._isClosedConnectionError(e)){
            return false;
        }
        if(this._isConnectionError(e)) {
            return false;
        }

        return true;
    }

    _isClosedConnectionError(e) {
        return e.payload && e.payload.error && e.payload.error.message && e.payload.error.message === "Closed connection";
    }

    _isBrokerNotConnectedError(e) {
        // TODO
        return false;
    }

    _isGroupCoordinatorNotFoundError(e) {
        return e.payload && e.payload.error && e.payload.error.name && e.payload.error.name === "KafkaJSGroupCoordinatorNotFound";
    }

    _isLeaderNotAvailableError(e) {
        return e.payload && e.payload.error && e.payload.error.originalError && e.payload.error.originalError.type && e.payload.error.originalError.type === "LEADER_NOT_AVAILABLE";
    }

    _isConnectionTimeoutError(e) {
        return e.payload && e.payload.error && e.payload.error.message && e.payload.error.message === "Connection timeout";
    }

    _isConnectionError(e) {
        return e.payload && e.payload.error && e.payload.error.originalError && e.payload.error.originalError.name === "KafkaJSConnectionError";
    }

    _isBrokerNotFoundError(e) {
        return e.payload && e.payload.error && e.payload.error.originalError && e.payload.error.originalError.name && e.payload.error.originalError.name === "KafkaJSBrokerNotFound";
    }

    _isRetryable(e) {
        return e.payload && e.payload.error && e.payload.error.retriable;
    }

    _isEConnRefusedError(e) {
        return (e.payload && e.payload.error && e.payload.error.originalError && e.payload.error.originalError.code && e.payload.error.originalError.code === "ECONNREFUSED") ||
               (e.payload && e.payload.error && e.payload.error.originalError && e.payload.error.originalError.originalError && e.payload.error.originalError.originalError.code === "ECONNREFUSED");
    }

    renderModal(e) {
        if (document.getElementById("modal-consumer-crash-error")) {
            return;
        }

        let modal = new Modal(`<div class='modal-error-message' id='modal-consumer-crash-error'><p class="error">${e.payload.error.message}</p></div><br /><div class="flex">
    <button id="modal-error-crash-close" style="width:100%;"><i class="fas fa-times"></i> Close</button>
</div>`);

        document.getElementById("modal-error-crash-close").onclick = () => {
            modal.close();
        }
    }

}

module.exports = {ConsumerCrashHandler}