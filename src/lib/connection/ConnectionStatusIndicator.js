const {ConnectionStatus} = require("./ConnectionStatus");

class ConnectionStatusIndicator {

    connectionDriver;
    element;
    retry_attempts;


    /**
     * @param {Router} app
     * @param {ConnectionDriver} connectionDriver
     */
    constructor(app, connectionDriver) {
        this.connectionDriver = connectionDriver;
        this.element          = document.createElement('div');
        this.retry_attempts = 0;
        this._initSubscriptions(app);

    }

    _initSubscriptions(app) {
        app.subscribe('connection.retry-started', () => {
            this.element.innerText = "Connection Lost - Reconnecting";
            this.element.className="connection-status retry";
        });
        app.subscribe('connection.lost', () => {
            this.element.innerText = "Disconnected";
            this.element.className="connection-status disconnected";
        });
        app.subscribe('connection.connected', () => {
            this.element.innerText = "Connected";
            this.element.className="connection-status connected";
        });
    }

    useConnectionStatus() {
        let status_bar = document.getElementById("connection-status-bar");
        if (status_bar && status_bar.innerText === "") {
            status_bar.appendChild(this.element);
        }
    }

}

module.exports = {ConnectionStatusIndicator};