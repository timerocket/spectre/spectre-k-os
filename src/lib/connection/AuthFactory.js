const {NoAuth}        = require('./auth-type/NoAuth');
const {ConfluentAuth} = require('./auth-type/ConfluentAuth');
const {SaslAuth}      = require('./auth-type/SaslAuth');
const {AwsIamAuth}    = require('./auth-type/AwsIamAuth');
const {SshAuth}       = require('./auth-type/SshAuth');
const {K8sAuth}       = require('./auth-type/K8sAuth');
const {AivenAuth}     = require('./auth-type/AivenAuth');
const {PortGiver}     = require('../PortGiver');

class AuthFactory {

    constructor() {
        this._sshPortGiver = new PortGiver(28000);
        this._k8sPortGiver = new PortGiver(27000);
    }

    /**
     * @param {*} authType
     * @param {*} config
     * @returns {{getConnection: function, getConfig: function }}
     */
    getAuth(authType, config) {

        const map = {
            'none'     : NoAuth,
            'plain'    : SaslAuth,
            'confluent': ConfluentAuth,
            'aws-iam'  : AwsIamAuth,
            'aws-ssh'  : SshAuth,
            'gcp-ssh'  : SshAuth,
            'azure-ssh': SshAuth,
            'k8s'      : K8sAuth,
            'aiven'    : AivenAuth,
        }

        const authClass = map[authType];

        const portGiverMap = {
            'aws-ssh'  : this._sshPortGiver,
            'gcp-ssh'  : this._sshPortGiver,
            'azure-ssh': this._sshPortGiver,
            'k8s'      : this._k8sPortGiver,
        }

        const portGiver = portGiverMap[authType];

        return new authClass(config, portGiver);
    }
}

module.exports = {AuthFactory}