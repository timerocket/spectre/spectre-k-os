
class NoAuth {

    /**
     * @private
     */
    config;

    constructor(config) {
        this.config = config
    }

    async getConnection(connection_object) {
        return connection_object;
    }

    /**
     * @returns {ConnectionDetails}
     */
    getConfig() {
        return this.config;
    }

}

module.exports = { NoAuth }