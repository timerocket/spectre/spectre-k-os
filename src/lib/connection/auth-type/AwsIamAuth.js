class AwsIamAuth {

    /**
     * @private
     */
    config;

    constructor(config) {
        this.config = config
    }

    async getConnection(connection_object) {
        return {
            ...connection_object,
            ssl : true,
            sasl: {
                mechanism            : 'aws',
                authorizationIdentity: this.config.aws_iam_role,
                accessKeyId          : this.config.aws_iam_access_key,
                secretAccessKey      : this.config.aws_iam_secret_access_key
            }
        }
    }

    getConfig() {
        return this.config;
    }

}

module.exports = {AwsIamAuth}