const net = require("net");
const {SSHDriver} = require('../SSHDriver');

class SshAuth {

    /**
     * @private
     */
    config;

    defaultPort;

    /**
     * @type {PortGiver}
     * @private
     */
    _portGiver;

    /**
     * @param {ConnectionDetails} config
     * @param {PortGiver} portGiver
     */
    constructor(config, portGiver) {
        this.config = config
        this.defaultPort = portGiver.getNextPort();
        this._portGiver = portGiver;
        this._serial = Math.random().toString(36).substring(7);
    }

    getNextPort() {
        return this._portGiver.getNextPort();
    }

    async getConnection(connection_object) {
        return connection_object;
    }

    getConfig() {
        let uri = this.config.connectUri;
        const uriParts = uri.split(':');
        if(!net.isIP(uriParts[0])) {
            uri = `127.0.0.1:${this.defaultPort}`
        }
        return {
            ...this.config,
            connectUri: uri
        };
    }

}

module.exports = { SshAuth }