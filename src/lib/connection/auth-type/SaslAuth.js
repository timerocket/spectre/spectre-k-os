class SaslAuth {

    /**
     * @private
     * @type {ConnectionDetails}
     */
    config;

    constructor(config) {
        this.config = config
    }

    async getConnection(connection_object) {

        let connection = {
            ...connection_object,
            sasl: {
                mechanism: this.config.sasl_mechanism || 'plain',
                username : this.config.username,
                password : this.config.password
            },
        }

        if(this.config.ssl_enabled){
            connection.ssl = true;

            if(this.config.ssl_self_signed) {
                connection.ssl = {
                    rejectUnauthorized: false,
                }
            }
        }

        return connection;
    }

    getConfig() {
        return this.config;
    }

}

module.exports = {SaslAuth}