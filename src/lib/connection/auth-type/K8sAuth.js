const net = require("net");
const { K8sDriver } = require("../K8sDriver");

class K8sAuth {

    /**
     * @private
     */
    config;

    defaultPort;

    /**
     * @type {PortGiver}
     * @private
     */
    _portGiver;

    /**
     * @param {ConnectionDetails} config
     * @param {PortGiver} portGiver
     */
    constructor(config, portGiver) {
        this.config = config;
        this.defaultPort = portGiver.getNextPort();
        this._portGiver = portGiver;
    }

    getNextPort() {
        return this._portGiver.getNextPort();
    }

    async getConnection(connection_object) {
        let connection = {
            ...connection_object,
        }
        if(this.config.k8s_ssl_enabled){
            connection.ssl = true;
        }
        if(this.config.k8s_username && this.config.k8s_password){
            connection.sasl = {
                mechanism: this.config.k8s_sasl_mechanism || 'plain',
                username : this.config.k8s_username,
                password : this.config.k8s_password
            }
        }
        return connection;
    }

    getConfig() {
        let uri = this.config.connectUri;
        const uriParts = uri.split(':');
        if(!net.isIP(uriParts[0])) {
            uri = `127.0.0.1:${this.defaultPort}`
        }
        return {
            ...this.config,
            connectUri: uri
        };
    }

}

module.exports = { K8sAuth }