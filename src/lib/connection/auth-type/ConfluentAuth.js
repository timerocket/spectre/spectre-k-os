class ConfluentAuth {

    /**
     * @private
     */
    config;

    constructor(config) {
        this.config = config
    }

    async getConnection(connection_object) {
        return {
            ...connection_object,
            ssl : true,
            sasl: {
                mechanism: 'confluent',
                username : this.config.username,
                password : this.config.password
            }
        }
    }

    getConfig() {
        return this.config;
    }

}

module.exports = {ConfluentAuth}