class AivenAuth {

    /**
     * @type {ConnectionDetails}
     * @private
     */
    config;

    /**
     * @type {ConnectionDetails}
     * @param config
     */
    constructor(config) {
        this.config = config
    }

    readFileSync(f) {
        if(fs.existsSync(f)) {
            return fs.readFileSync(f);
        }else {
            throw Error(`Could read find file at path ${f}`);
        }
    }

    async getConnection(connection_object) {

        const ca                 = this.config.aiven_ca_path ? this.readFileSync(this.config.aiven_ca_path) : undefined;
        const key                = this.config.aiven_key_path ? this.readFileSync(this.config.aiven_key_path) : undefined;
        const cert               = this.config.aiven_cert_path ? this.readFileSync(this.config.aiven_cert_path) : undefined;

        const rejectUnauthorized = !ca;

        let config = {
            ...connection_object
        }

        if(ca && key && cert) {
            config = {
                ...config,
                ssl : {
                    rejectUnauthorized,
                    ca,
                    key,
                    cert
                },
            }
        }

        if(this.config.aiven_user && this.config.aiven_user.trim() && this.config.aiven_password && this.config.aiven_password.trim()) {
            config = {
                ...config,
                sasl: {
                    mechanism: 'plain',
                    username : this.config.aiven_user,
                    password : this.config.aiven_password,
                }
            }
            if(config.ssl) {
                config.ssl.rejectUnauthorized = rejectUnauthorized
            }else{
                config.ssl = {
                    rejectUnauthorized
                }
            }

            if(!config.ssl.rejectUnauthorized) {
                config.ssl.ca = ca;
            }

        }

        return config;
    }

    getConfig() {
        return this.config;
    }

}

module.exports = {AivenAuth}
