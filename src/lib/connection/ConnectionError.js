
class ConnectionError extends Error {

    /**
     * @private
     */
    _errorType;

    get errorType() {
        return this._errorType;
    }

    /**
     * @param {string} message
     * @param {ConnectionErrorType} errorType
     */
    constructor(message, errorType) {
        super(message);
    }
}