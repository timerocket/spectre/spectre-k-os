/**
 * @typedef {'SUCCESS' | 'TIMEOUT' | 'UNKNOWN_FAILURE' | 'CANCELED'} ConnectionResult
 */

class ConnectionResult {

    static get SUCCESS() {
        return 'SUCCESS'
    }

    static get TIMEOUT() {
        return 'TIMEOUT'
    }

    static get UNKNOWN_FAILURE() {
        return 'UNKNOWN_FAILURE'
    }

    static get SKIPPED() {
        return 'SKIPPED'
    }

    static get CANCELED() {
        return 'CANCELED'
    }
}

module.exports = {ConnectionResult}