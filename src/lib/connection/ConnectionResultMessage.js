class ConnectionResultMessage {

    /**
     * @type {"SUCCESS" | "TIMEOUT" | "UNKNOWN_FAILURE" | "CANCELLED"}
     */
    result;

    /**
     * @type {string}
     */
    message;

    /**
     * @param {ConnectionResult} result
     * @param {string} message
     */
    constructor(result, message) {
        this.result  = result;
        this.message = message;
    }

}

module.exports = {ConnectionResultMessage};