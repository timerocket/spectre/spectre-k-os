/**
 * @typedef {"connecting" | "connected" | "disconnected" | "reconnecting"} ConnectionStatusType
 */

class ConnectionStatus {
    static get CONNECTING() { return "connecting" }
    static get CONNECTED() { return "connected" }
    static get DISCONNECTED() { return "disconnected" }
    static get RECONNECTING() { return "reconnecting" }
}

module.exports = {ConnectionStatus}