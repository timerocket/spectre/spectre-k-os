const {ConnectionDriver}        = require('./ConnectionDriver');
const {ConnectionStatus}        = require('./ConnectionStatus');
const {ConnectionResultMessage} = require('./ConnectionResultMessage');
const {ConnectionResult}        = require('./ConnectionResult');
const {AuthFactory}             = require('./AuthFactory');

class Connection {

    /**
     * @private
     * @type {Map<string, ConnectionDriver>}
     */
    _disconnectingDrivers;

    /**
     * @private
     * @type {ConnectionDriver}
     */
    _currentConnectionDriver;

    _settings;

    /**
     * @type {AuthFactory}
     * @private
     */
    _authFactory;

    /**
     * @type {Router}
     * @private
     */
    _app;

    constructor(app, settings) {
        this._app = app;
        this._authFactory             = new AuthFactory();
        this._currentConnectionDriver = new ConnectionDriver(app, settings, this._authFactory);
        this._settings                = settings;
        this._disconnectingDrivers    = new Map();
        this._initSubscriptions(app);
    }

    /**
     * @param {Router} app
     * @private
     */
    _initSubscriptions(app) {
        app.subscribe('connection.lost',() =>{
            this.disconnect();
        });
    }

    getDriver() {
        return this._currentConnectionDriver;
    }

    disconnect() {
        const previousDriver          = this._currentConnectionDriver;
        this._currentConnectionDriver = new ConnectionDriver(this._app, this._settings, this._authFactory);
        this._disconnectingDrivers.set(previousDriver.key, previousDriver);
        this._disconnectDriver(previousDriver);
    }

    _disconnectDriver(driver) {
        this._doDisconnectDriver(driver).then(() => {
            this._disconnectingDrivers.delete(driver.key);
        })
    }

    /**
     * @param {ConnectionDriver} driver
     * @returns {Promise<void>}
     * @private
     */
    async _doDisconnectDriver(driver) {
        driver.connectionCancellationRequested = true;
        while (driver.connectionStatus === ConnectionStatus.CONNECTING) {
            await new Promise(resolve => {
                setTimeout(resolve, 200)
            });
        }
        await driver.disconnect();
    }

    /**
     * @param {ConnectionDetails} config
     */
    addRecentConnection(config) {
        this._currentConnectionDriver.addRecentConnection(config);
    }

    getRecentConnections() {
        return this._currentConnectionDriver.recentConnections;
    }

    saveRecentConnections() {
        this._currentConnectionDriver.saveRecentConnections();
    }

    getConnectionDetails() {
        return this._currentConnectionDriver.getConnectionDetails();
    }

    /**
     * @returns {KafkaDriver}
     */
    getKafkaDriver() {
        return this._currentConnectionDriver.getKafkaDriver();
    }

    /**
     * @param {ConnectionDetails} config
     * @returns {Promise<ConnectionResultMessage>}
     */
    testConnection(config) {
        return this._currentConnectionDriver.testConnection(config);
    }

    /**
     * @param {ConnectionDetails} config
     * @returns {Promise<ConnectionResultMessage>}
     */
    async connect(config) {
        let connectionDriver = this._currentConnectionDriver;
        if (connectionDriver.connectionStatus === ConnectionStatus.CONNECTING) {
            this.disconnect()
            connectionDriver = this._currentConnectionDriver;
        }
        const message = await connectionDriver.connect(config);
        if (connectionDriver.connectionCancellationRequested) {
            return new ConnectionResultMessage(ConnectionResult.CANCELED, "Connection was cancelled");
        } else {
            return message;
        }
    }
}

module.exports = {Connection}