const k8s = require("@kubernetes/client-node");
const net = require('net');

class K8sDriver {

    /**
     * @type {boolean}
     */
    isConnected;

    constructor() {
        this.isConnected    = false;
        this._forwardedPods = [];
    }

    /**
     * @private
     * @type {{name: string, namespace: string, server: *, websocket: *, localPort: number}[]}
     */
    _forwardedPods;

    _disconnectIntervalId;

    /**
     * @param {K8sAuth} auth
     * @returns {number|*}
     * @private
     */
    _getNextLocalPort(auth) {
        if (this._forwardedPods.length === 0) {
            return auth.defaultPort;
        } else {
            return auth.getNextPort();
        }
    }

    /**
     * @param {K8sAuth} auth
     * @returns {Promise<void>}
     */
    async connect(auth) {

        const config = auth.getConfig();
        let k8sApi;
        let kc;
        try {
            kc = new k8s.KubeConfig();
            kc.loadFromDefault();
            kc.setCurrentContext(config.k8s_context);
            k8sApi = kc.makeApiClient(k8s.CoreV1Api);
        }catch (e) {
            throw new Error(`Invalid kube context ${config.k8s_context}`);
        }

        let service;
        try {
            service = await k8sApi.readNamespacedService(config.k8s_service, config.k8s_namespace);
        } catch (e) {
            if(e.code === "ENOTFOUND") {
                throw e;
            }else if(e.statusCode === 404) {
                throw new Error(`Service ${config.k8s_service} does not exist in namespace ${config.k8s_namespace}`);
            }else if(e.response && e.response.body && e.response.body.message){
                throw new Error(`Unexpected error: ${e.response.body.message}`)
            }else{
                throw e;
            }
        }

        try {
            const selectors    = service.body.spec.selector;
            let labelSelectors = [];
            for (const key in selectors) {
                const value = selectors[key];
                labelSelectors.push(`${key}=${value}`);
            }
            const labelSelectorString = labelSelectors.join(',');
            const podsResponse        = await k8sApi.listNamespacedPod(config.k8s_namespace, undefined, undefined, undefined, undefined, labelSelectorString);

            const podNames = podsResponse.body.items.map(pod => pod.metadata.name);

            for (const podName of podNames) {
                if(!this._hasForwardedPod(podName, config)){
                    await this._forwardPod(podName, kc, config, config.k8s_port, auth);
                }
            }
        } catch (e) {
            throw new Error(`Could not connect to pods for ${config.k8s_service} in namespace ${config.k8s_namespace}`);
        }
    }

    /**
     * @param {string} podName
     * @param {k8s.KubeConfig} kc
     * @param {ConnectionDetails} config
     * @param {number} targetPort
     * @param {K8sAuth} auth
     * @returns {Promise<void>}
     * @private
     */
    async _forwardPod(podName, kc, config, targetPort, auth){
        const localPort = this._getNextLocalPort(auth);

        const forward = new k8s.PortForward(kc);

        const server = net.createServer(async (socket) => {
            console.log(`Forwarding from port ${localPort} to ${targetPort} for pod ${podName} in namespace ${config.k8s_namespace}`);
            try {
                await forward.portForward(config.k8s_namespace, podName, [targetPort], socket, null, socket);
            }catch (e) {
                console.warn(e);
            }
        });

        server.listen(localPort, '127.0.0.1');

        this._forwardedPods.push(
            {
                server,
                name     : podName,
                namespace: config.k8s_namespace,
                localPort: localPort
            }
        )
    }

    _hasForwardedPod(podName, config) {
        for(const forwardedPod of this._forwardedPods){
            if(forwardedPod.namespace === config.k8s_namespace && forwardedPod.name === podName){
                return true;
            }
        }
        return false;
    }

    async disconnect() {
        try {
            await this._doDisconnect();
            if(this._disconnectIntervalId !== undefined) {
                clearInterval(this._disconnectIntervalId);
            }
        }catch (e){
            let tryHardCount = 0;
            this._disconnectIntervalId = setInterval(async () => {
                try {
                    tryHardCount++;
                    if(tryHardCount >= 20){
                        clearInterval(this._disconnectIntervalId);
                        return;
                    }
                    await this._doDisconnect();
                    clearInterval(this._disconnectIntervalId);
                }catch (e) {
                    console.log("Couldn't disconnect from pods");
                }
            }, 5000);
            console.warn(`Pod disconnect failed`, e);
        }
    }
    async _doDisconnect() {
        for (const forwardedPod of this._forwardedPods) {
            await new Promise((resolve, reject) => {
                let didComplete = false;
                forwardedPod.server.close((err) => {
                    if(!didComplete) {
                        didComplete = true;
                        if (err) {
                            console.warn(`Pod disconnect failed`, err);
                            resolve();
                        } else {
                            resolve();
                        }
                    }
                })
                setTimeout(resolve => {
                    if(!didComplete){
                        didComplete = true;
                        reject(new Error('Timed out'));
                    }
                }, 5000);
            });
        }
        this._forwardedPods = [];
    }

    _getForwardedUriForBroker(broker) {
        const hostParts = broker.host.split('.');
        const brokerHost = hostParts[0];
        for (const forwardedPod of this._forwardedPods) {
            if(forwardedPod.name === brokerHost) {
                return `127.0.0.1:${forwardedPod.localPort}`
            }
        }
    }

    getForwardedUris(brokers) {
        const newUris = [];
        for(const broker of brokers) {
            newUris.push(this._getForwardedUriForBroker(broker));
        }
        return newUris;
    }

}

module.exports = {K8sDriver}