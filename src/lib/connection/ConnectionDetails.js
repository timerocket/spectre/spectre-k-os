class ConnectionDetails {

    inputUri;

    connectUri;

    username;

    password;

    ssl_enabled;

    ssl_self_signed;

    sasl_mechanism;

    confluent_username;

    confluent_password;

    apiKey;

    nickname;

    aws_iam_role;

    aws_iam_access_key;

    aws_iam_secret_access_key;

    ssh_path;

    ssh_username;

    ssh_password;

    ssh_host;

    ssh_port;

    ssh_passphrase;

    ssh_kafka_username;

    ssh_kafka_password;

    ssh_use_rsa;

    ssh_rsa;

    k8s_context;

    k8s_service;

    k8s_namespace;

    k8s_port;

    k8s_username;

    k8s_password;

    k8s_sasl_mechanism;

    k8s_ssl_enabled;

    aiven_user;

    aiven_password;

    aiven_ca_path;

    aiven_key_path;

    aiven_cert_path;

    auth_type = "none";

    /**
     * @type {string[]} ({host}:{port})
     */
    clusterBrokerUris = [];

    /**
     * @param {*} config
     */
    constructor(config) {
        for (const k in config) {
            this[k] = config[k];
        }
        if (!this.connectUri) {
            this.connectUri = this.inputUri;
        }
    }

    shouldPromptForSSHPassword() {
        return this.auth_type.includes('-ssh') && !this.ssh_password && !this.ssh_path;
    }

    shouldPromptForRSAKey() {
        return this.ssh_use_rsa;
    }

    shouldPrompt() {
        return this.shouldPromptForSSHPassword() || this.shouldPromptForRSAKey();
    }

}

module.exports = {ConnectionDetails}