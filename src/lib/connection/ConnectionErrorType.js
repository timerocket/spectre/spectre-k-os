class ConnectionErrorType {

    static get TIMEOUT() {
        return 'TIMEOUT'
    }

    static get UNKNOWN_FAILURE() {
        return 'UNKNOWN_FAILURE'
    }
}

module.exports = {ConnectionErrorType}