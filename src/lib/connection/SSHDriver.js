const fs        = require('fs');
const sshTunnel = require('tunnel-ssh');

class SSHDriver {

    isConnected;

    /**
     * @private
     */
    _isLocked;

    _addMissingTunnelsLocked;

    /**
     * @private
     * @type {{localPort: number, targetHost: string, targetPort, tunnel}[]}
     */
    _tunnelsCreated;

    constructor() {
        this.isConnected     = false;
        this._isLocked       = false;
        this._addMissingTunnelsLocked = false;
        this._tunnelsCreated = [];
    }

    /**
     * @param {SshAuth} auth
     * @returns {number|*}
     * @private
     */
    _getNextLocalPort(auth) {
        if (this._tunnelsCreated.length === 0) {
            return auth.defaultPort;
        } else {
            return auth.getNextPort();
        }
    }

    _getCreatedTunnelForUri(uri) {
        for (const tunnel of this._tunnelsCreated) {
            if (`${tunnel.targetHost}:${tunnel.targetPort}` === uri) {
                return tunnel;
            }
        }
        return null;
    }

    async addMissingTunnels(uris, auth) {
        if(this._addMissingTunnelsLocked){
            while(this._addMissingTunnelsLocked){
                await new Promise(resolve => {
                    setTimeout(resolve, 100);
                })
            }
        }
        this._addMissingTunnelsLocked = true;

        const newUris = [];
        for (const uri of uris) {
            let createdTunnel = this._getCreatedTunnelForUri(uri);
            if(!createdTunnel){
                await this.addTunnel(uri, auth);
                createdTunnel = this._getCreatedTunnelForUri(uri);
            }
            if(!createdTunnel){
                newUris.push(uri);
            }else{
                newUris.push(`127.0.0.1:${createdTunnel.localPort}`);
            }

        }
        this._addMissingTunnelsLocked = false;
        return newUris;
    }

    readFileAsync(f) {
        try {
            return fs.readFileSync(f);
        } catch (e) {
            throw e;
        }
    }

    /**
     * @param {string} uri
     * @param {SshAuth} auth
     * @returns {Promise<void>}
     */
    async addTunnel(uri, auth) {
        const config = auth.getConfig();
        if (this._isLocked) {
            return;
        }
        this._isLocked = true;

        const uriParts = uri.split(':');

        const kafkaPort    = uriParts[1] || 9092;
        const tunnelConfig = {
            username : config.ssh_username,
            host     : config.ssh_host,
            port     : config.ssh_port,
            dstHost  : uriParts[0],
            dstPort  : kafkaPort,
            localHost: '127.0.0.1',
            localPort: this._getNextLocalPort(auth),
            keepAlive: true
        };

        if (!config.ssh_password && !config.ssh_path) {
            this._isLocked = false;
            throw new Error('Missing ssh password for private key path');
        }

        try {
            let tunnel;
            if (config.ssh_path) {
                tunnel = await this._connectSSHTunnel(
                    {
                        ...tunnelConfig,
                        privateKey: this.readFileAsync(config.ssh_path),
                        passphrase: config.ssh_passphrase ? config.ssh_passphrase : undefined
                    }
                )
            } else if (config.ssh_use_rsa) {
                tunnel = await this._connectSSHTunnel(
                    {
                        ...tunnelConfig,
                        Password: `${config.ssh_password}${config.ssh_rsa}`
                    }
                )
            } else {
                tunnel = await this._connectSSHTunnel(
                    {
                        ...tunnelConfig,
                        Password: `${config.ssh_password}`
                    }
                )
            }
            this.isConnected = true;
            this._isLocked   = false;
            this._tunnelsCreated.push(
                {
                    localPort : tunnelConfig.localPort,
                    targetPort: tunnelConfig.dstPort,
                    targetHost: tunnelConfig.dstHost,
                    tunnel,
                }
            )
        } catch (e) {
            this.isConnected = false;
            this._isLocked   = false;
            throw e;
        }
    }

    /**
     * @param {SshAuth} auth
     * @returns {Promise<void>}
     */
    async connect(auth) {
        await this.disconnect();

        const useConfig = await this._getConfig(auth);

        await this.addTunnel(useConfig.inputUri, auth);
    }

    /**
     * @param {*} tunnelConfig
     * @private
     */
    _connectSSHTunnel(tunnelConfig) {
        return new Promise(async (resolve, reject) => {
            let didComplete = false;
            sshTunnel(tunnelConfig, (error, server) => {
                if (error) {
                    reject(error);
                } else {
                    server.on('connection', () => {
                        if (!didComplete) {
                            didComplete = true;
                            resolve(server);
                        }
                    });
                    server.on('error', (err) => {
                        if (!didComplete) {
                            didComplete = true;
                            reject(err);
                        }
                    });
                    setTimeout(() => {
                        if (!didComplete) {
                            didComplete = true;
                            resolve(server);
                        }
                    }, 5000);
                }
            })
        })
    }

    async disconnect() {
        for(const tunnelCreated of this._tunnelsCreated){
            tunnelCreated.tunnel.close();
        }
        this._tunnelsCreated = [];
        this.isConnected = false;
    }

    /**
     * @param config
     * @returns {Promise<ConnectionDetails>}
     * @private
     */
    _getConfig(auth) {
        const config = auth.getConfig();
        const useConfig = {...config};
        return new Promise(resolve => {
            if (!useConfig.ssh_password && !useConfig.ssh_path) {

                let modal = new Modal(`<div class=""><input id="connect-ssh-password" style="width:100%;" class="full" type="password" placeholder="SSH Password"><br /><br /><div class="flex"><button id="ssh_connect" style="width:100%;margin-right:8px;" class="button full">Connect</button><button id="ssh_close" style="width:100%;" class="button full"><i class="fas fa-times"></i> Cancel</button></div></div></div>`, {class: "wide"});

                console.log("MADE MODAL", modal);
                let connect_element     = document.getElementById('ssh_connect');
                connect_element.onclick = () => {
                    useConfig.ssh_password = document.getElementById("connect-ssh-password").value;
                    modal.close();
                    resolve(useConfig);
                }

                let close_element     = document.getElementById('ssh_close');
                close_element.onclick = () => {
                    modal.close();
                    resolve(useConfig);
                }

            } else {
                resolve(useConfig);
            }
        });
    }

}

module.exports = {SSHDriver}
