const {ConnectionResult}        = require("./ConnectionResult");
const {SSHDriver}               = require('./SSHDriver');
const {KafkaDriver}             = require('./KafkaDriver');
const {ConnectionDetails}       = require('./ConnectionDetails');
const {AuthFactory}             = require('./AuthFactory');
const {K8sDriver}               = require('./K8sDriver');
const {ConnectionStatus}        = require('./ConnectionStatus');
const {ConnectionResultMessage} = require('./ConnectionResultMessage');

class ConnectionDriver {

    /**
     * @type {KafkaDriver}
     * @private
     */
    _kafkaDriver;

    /**
     * @private
     * @type {SSHDriver}
     */
    _sshDriver;

    /**
     * @private
     * @type {K8sDriver}
     */
    _k8sDriver;

    /**
     * @type {ConnectionDetails}
     * @private
     */
    _connectionDetails;

    /**
     * @type {ConnectionDetails[]}
     * @public
     */
    recentConnections;

    /**
     * @type {"connecting" | "disconnected" | "connected"}
     */
    connectionStatus;

    /**
     * @type {boolean}
     */
    connectionCancellationRequested;

    key;

    /**
     * @type {AuthFactory}
     * @private
     */
    _authFactory;

    /**
     * @type {Router}
     * @private
     */
    _app;

    /**
     * @type {ConnectionDetails}
     * @private
     */
    _config;

    /**
     * @param {Router} app
     * @param {SettingsDriver} settings
     * @param {AuthFactory} authFactory
     */
    constructor(app, settings, authFactory) {
        this._app               = app;
        this._connectionDetails = new ConnectionDetails({});
        this._sshDriver         = new SSHDriver();
        this._k8sDriver         = new K8sDriver();
        this._kafkaDriver       = new KafkaDriver(app, this._sshDriver, this._k8sDriver, settings);
        this.recentConnections  = [];
        let json_saved          = window.localStorage.getItem('_spectre_es_gui_saved_connections');
        if (json_saved) {
            this.recentConnections = JSON.parse(json_saved).map(rawDetails => new ConnectionDetails(rawDetails));
        }
        this.connectionStatus                = ConnectionStatus.DISCONNECTED;
        this.connectionCancellationRequested = false;
        this.key                             = Math.random().toString(36).substring(7);
        this._authFactory                    = authFactory;
        this._initSubscriptions(app);
    }

    _initSubscriptions(app) {
        this._app.subscribe('kafka.connection.retry-started', () => {
            if (!this.connectionCancellationRequested) {
                this.connectionStatus = ConnectionStatus.RECONNECTING;
                this._app.publish('connection.retry-started').then();
            }
        });
        this._app.subscribe('kafka.connection.connected', () => {
            if (!this.connectionCancellationRequested) {
                this.connectionStatus = ConnectionStatus.CONNECTED;
                this._app.publish('connection.connected').then();
            }
        })
        this._app.subscribe('kafka.connection.lost', async () => {
            if (!this.connectionCancellationRequested) {
                for (let i = 0; i < 2; i++) {
                    if (!this.connectionCancellationRequested) {
                        await this.disconnect();
                        const message = await this.connect(this._config);
                        if (message.result === ConnectionResult.SUCCESS) {
                            return;
                        }
                    } else {
                        return;
                    }
                }
                if (!this.connectionCancellationRequested) {
                    this._app.publish('connection.lost').then();
                }
            }
        });
    }

    /**
     * @returns {KafkaDriver}
     */
    getKafkaDriver() {
        return this._kafkaDriver;
    }

    /**
     * @param {ConnectionDetails } config
     * @returns {Promise<ConnectionResultMessage>}
     * @throws {ConnectionError}
     */
    async connect(config) {
        try {
            this.connectionStatus = ConnectionStatus.CONNECTING;
            const auth            = this._authFactory.getAuth(config.auth_type, config);
            await this._createTunnels(auth);
            const message = await this._kafkaDriver.connect(auth);
            if (message.result === ConnectionResult.SUCCESS) {
                this.connectionStatus = ConnectionStatus.CONNECTED;
                this._config          = config;
                this._app.publish('connection.connected').then();
            } else {
                this.connectionStatus = ConnectionStatus.DISCONNECTED;
            }

            return message;
        } catch (e) {
            this.connectionStatus = ConnectionStatus.DISCONNECTED;
            return new ConnectionResultMessage(ConnectionResult.UNKNOWN_FAILURE, e);
        }
    }

    /**
     * @param {NoAuth} auth
     * @returns {Promise<void>}
     * @private
     */
    async _createTunnels(auth) {
        await this._connectSSH(auth);
        await this._connectK8s(auth);
    }

    async disconnect() {
        await this.getKafkaDriver().disconnect();
        await this._sshDriver.disconnect();
        await this._k8sDriver.disconnect();
    }

    /**
     * @param {ConnectionDetails} config
     * @returns {Promise<ConnectionResultMessage>}
     */
    async testConnection(config) {
        const auth = this._authFactory.getAuth(config.auth_type, config);
        await this._createTunnels(auth);
        return this._kafkaDriver.testConnection(auth);
    }

    async _connectSSH(auth) {
        if (!this._sshDriver.isConnected && auth.getConfig().auth_type.includes("-ssh")) {
            await this._sshDriver.connect(auth);
        }
    }

    async _connectK8s(auth) {
        if (!this._k8sDriver.isConnected && auth.getConfig().auth_type === "k8s") {
            await this._k8sDriver.connect(auth);
        }
    }

    getConnectionDetails() {
        return {
            ...this._connectionDetails,
            recent_connections: this.recentConnections,
            save_connection   : null,
        };
    }

    addRecentConnection(details) {
        let connection_details = Object.assign({}, {
            ...details,
            clusterBrokerUris: this.getKafkaDriver().getClusterBrokerUris()
        });

        let has_connection = false;

        if (!has_connection) {
            this.recentConnections.push(connection_details);
        }
        this.saveRecentConnections();
    }

    saveRecentConnections() {
        window.localStorage.setItem('_spectre_es_gui_saved_connections', JSON.stringify(this.recentConnections))
    }
}

module.exports = {ConnectionDriver}