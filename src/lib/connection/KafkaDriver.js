const {ConnectionStatus}        = require("./ConnectionStatus");
const {ConnectionResultMessage} = require("./ConnectionResultMessage");
const {Kafka}                   = require("kafkajs");
const {ClusterBrokers}          = require('../ClusterBrokers');
const {ConnectionResult}        = require('./ConnectionResult');
const {jsonSize}                = require('../util');

/**
 * @typedef {name: string, partitions: number} TopicData
 */

class KafkaDriver {
  consumerClient;

  producerClient;

  adminClient;

  admin;

  producer;

  /**
   * @type string
   */
  inputUri;

  /**
   * @type {string}
   */
  consumerGroupId;

  /**
   * @type {ClusterBrokers}
   */
  clusterBrokers;

  /**
   * @private
   */
  _connectionConfig;

  /**
   * @private
   * @type {SSHDriver}
   */
  _sshDriver;

  /**
   * @private
   * @type {K8sDriver}
   */
  _k8sDriver;

  /**
   * @private
   */
  _settingsDriver;

  consumers;

  messages;

  topicAges;

  /**
   * @type {number}
   */
  cacheUsedSize;

  /**
   *
   * @private
   */
  _brokers;

  /**
   * @type {Router}
   * @private
   */
  _app;

  /**
   * @type {NoAuth}
   * @private
   */
  _auth;

  /**
   * @private
   * @type {number}
   */
  _connectionStatusIntervalId;

  /**
   * @type {ConnectionStatusType}
   * @private
   */
  _connectionStatus;

  /**
   * @type {TopicData[]}
   * @private
   */
  _topics;

  /**
   * @type {boolean}
   * @private
   */
  _didInit;

  _disconnectRequested;

  _advertisedBrokers;

  static get MAX_CONNECTION_RETRIES() {
    return 10;
  }

  constructor(app, sshDriver, k8sDriver, settingsDriver) {
    this._app            = app;
    this.consumerGroupId = window.localStorage.getItem("_spectre_consumer_group") || "_spectre_kafka_gui_messages_" + (((Math.random() * 1000000) | 0).toString()) + (((Math.random() * 1000000) | 0).toString()) + (((Math.random() * 1000000) | 0).toString());
    window.localStorage.setItem("_spectre_consumer_group", this.consumerGroupId);
    this._sshDriver           = sshDriver;
    this._k8sDriver           = k8sDriver;
    this._settingsDriver      = settingsDriver;
    this.messages             = {};
    this.consumers            = {};
    this.topicAges            = {};
    this.cacheUsedSize        = 0;
    this._topics              = [];
    this._disconnectRequested = false;
    this._connectionStatus    = ConnectionStatus.CONNECTING;
    this._advertisedBrokers   = [];
  }

  /**
   * @returns {ConnectionStatusType}
   */
  getConnectionStatus() {
    return this._connectionStatus;
  }

  /**
   * @param {NoAuth} auth
   * @returns {Promise<ConnectionResultMessage>}
   */
  async testConnection(auth) {
    return this._doReliableConnect(auth);
  }

  /**
   * @param {NoAuth} auth
   * @returns {Promise<ConnectionResultMessage>}
   */
  async connect(auth) {
    return this._connect(auth);
  }

  getConnectionNickname() {
    if (this._connectionConfig) {
      return this._connectionConfig.nickname;
    } else {
      return undefined;
    }
  }

  /**
   * @param {NoAuth} auth
   * @returns {Promise<ConnectionResultMessage>}
   */
  async _connect(auth) {
    if (JSON.stringify(this._connectionConfig) !== JSON.stringify(auth.getConfig())) {
      if (this._connectionConfig) {
        await this.disconnect();
      }
      return await this._doReliableConnect(auth);
    } else {
      return new ConnectionResultMessage(ConnectionResult.SUCCESS, "Success");
    }
  }

  /**
   * @param {NoAuth} auth
   * @returns {Promise<ConnectionResultMessage>}
   */
  async _doReliableConnect(auth) {
    try {
      for (let i = 1; i <= KafkaDriver.MAX_CONNECTION_RETRIES; i++) {

        const message = await this._doConnect(auth);
        if (message.result !== ConnectionResult.TIMEOUT) {
          return message;
        }
      }
      return new ConnectionResultMessage(ConnectionResult.TIMEOUT, "Timed out while trying to connect");
    } catch (e) {
      return new ConnectionResultMessage(ConnectionResult.UNKNOWN_FAILURE, e.message);
    }
  }

  /**
   * @returns {Promise<void>}
   * @private
   */
  async _updateConnectionStatus() {
    try {
      await this.listTopics(5000);
    } catch (e) {
      await this._fixConnection(e);
    }
  }

  async listTopics(timeout = 30000) {
    return new Promise((resolve, reject) => {
      let didComplete = false;
      this.admin.listTopics().then((topics) => {
        if (!didComplete) {
          didComplete = true;
          resolve(topics);
        }
      }).catch(async (e) => {
        if (!didComplete) {
          didComplete = true;
          reject(e);
        }
      });
      setTimeout(async () => {
        if (!didComplete) {
          didComplete = true;
          reject(new Error("Timed out"))
        }
      }, timeout);
    });
  }

  async onConsumerCrash(e) {
    await this._fixConnection(e);
  }

  async _fixConnection(e) {
    if (this._disconnectRequested || [ConnectionStatus.RECONNECTING, ConnectionStatus.DISCONNECTED, ConnectionStatus.CONNECTING].includes(this._connectionStatus)) {
      return;
    }
    if (!KafkaDriver.isDisconnectedError(e)) {
      if (e.originalError && e.originalError.name !== "KafkaJSConnectionClosedError") {
        console.error(e);
      }
      return;
    }
    this._connectionStatus = ConnectionStatus.RECONNECTING;
    clearInterval(this._connectionStatusIntervalId);
    this._app.publish('kafka.connection.retry-started').then();

    for (let i = 0; i < 5; i++) {
      if (this._disconnectRequested) {
        this._connectionStatus = ConnectionStatus.DISCONNECTED;
        return;
      }
      await this._doDisconnect();
      if (this._disconnectRequested) {
        this._connectionStatus = ConnectionStatus.DISCONNECTED;
        return;
      }
      const message = await this._doConnect(this._auth);
      if (message.result === ConnectionResult.SUCCESS) {
        this._app.publish('kafka.connection.connected').then();
        return;
      }
      this._connectionStatus = ConnectionStatus.RECONNECTING;
    }

    this._app.publish('kafka.connection.lost').then();
  }

  /**
   * @param {ConnectionStatusType} connectionStatus
   */
  setConnectionStatus(connectionStatus) {
    this._connectionStatus = connectionStatus;
  }

  static isDisconnectedError(e) {
    return e.name === "KafkaJSConnectionClosedError" ||
           (e.originalError && e.originalError.code === "ECONNREFUSED") ||
           (e.payload && e.payload.error && e.payload.error.name === "KafkaJSConnectionClosedError") ||
           (e.name === "KafkaJSConnectionError") ||
           e.message === "Timed out"
  }

  _initConnectionStatusInterval() {
    this._connectionStatusIntervalId = window.setInterval(async () => {
      await this._updateConnectionStatus();
    }, 5000)
  }

  /**
   * @param {NoAuth} auth
   * @returns {Promise<ConnectionResultMessage>}
   */
  async _doConnect(auth) {

    this._connectionStatus = ConnectionStatus.CONNECTING;
    this.inputUri          = auth.getConfig().inputUri
    const config           = auth.getConfig();
    this.clusterBrokers    = new ClusterBrokers(config);
    await this.clusterBrokers.init();

    console.log('TRY AND CONNECT');
    let connection_object = await this._getConnectionObject(auth);

    try {
      this.consumerClient = new Kafka(connection_object);
    } catch (e) {
      console.error("CONNECTION ERR", e);
      return new ConnectionResultMessage(ConnectionResult.UNKNOWN_FAILURE, e.message);
    }

    try {

      let didComplete = false;
      await new Promise((resolve, reject) => {
        this.producerClient = new Kafka(connection_object);
        this.adminClient    = new Kafka(connection_object);
        this.producer       = this.producerClient.producer();

        this.producer.connect()
            .then(() => {
              if (didComplete) {
                return new Error('Completed');
              }
              console.log('CREATED PRODUCER');
              this.admin = this.adminClient.admin();
              return this.admin.connect();
            })
            .then(() => {
              if (didComplete) {
                return new Error('Completed');
              }
              console.log('CREATED ADMIN');
              return this.admin.describeCluster();
            })
            .then((cluster) => {
              if (didComplete) {
                return new Error('Completed');
              }
              console.log(cluster);
              this._initConnectionStatusInterval();
              didComplete = true;
              resolve();
            })
            .catch(e => {
              if (!didComplete) {
                didComplete = true;
                reject(e);
              }
            });

        const timeoutSeconds = config.auth_type === "k8s" ? 5 : 30;
        setTimeout(() => {
          if (!didComplete) {
            didComplete = true;
            reject(new Error("Connection timeout"));
          }
        }, timeoutSeconds * 1000)

      })

    } catch (e) {

      this._connectionStatus = ConnectionStatus.DISCONNECTED;
      if (e.message === 'Connection timeout') {
        return new ConnectionResultMessage(ConnectionResult.TIMEOUT, e.message);
      } else {
        return new ConnectionResultMessage(ConnectionResult.UNKNOWN_FAILURE, e.message);
      }
    }

    this._connectionConfig = auth.getConfig();
    this._auth             = auth;
    this._connectionStatus = ConnectionStatus.CONNECTED;

    return new ConnectionResultMessage(ConnectionResult.SUCCESS, null);
  }

  shouldRestart() {
    return this._connectionStatus === ConnectionStatus.CONNECTED;
  }

  /**
   * @private
   * @param {NoAuth} auth
   * @returns {Promise<*>}
   * @private
   */
  async _getConnectionObject(auth) {
    const config = auth.getConfig();
    return auth.getConnection(
      {
        clientId          : '_spectre_kafka_gui_connection',
        brokers           : [config.connectUri],
        retry             : {
          restartOnFailure: async () => {
            return false
          },
          retries         : 0,
        },
        logCreator        : () => {
          return ({level, log}) => {
            if (log.message === "Connection timeout") {
              this.clusterBrokers.add(log.broker);
            } else {
              console.log(log);
            }
          }
        },
        getMetadataBrokers: async (brokers) => {
          const uris = brokers.map(broker => `${broker.host}:${broker.port}`);
          console.log(`Advertised brokers: `, uris);
          this._advertisedBrokers = brokers;

          if (config.auth_type.includes("-ssh")) {
            if (!this._brokers) {
              const uris    = brokers.map(broker => `${broker.host}:${broker.port}`);
              const newUris = await this._sshDriver.addMissingTunnels(uris, auth);
              this._brokers = this._getBrokersForUris(brokers, newUris);
            }
            return this._brokers;
          } else if (config.auth_type === "k8s") {
            if (!this._brokers) {
              const newUris = this._k8sDriver.getForwardedUris(brokers);
              this._brokers = this._getBrokersForUris(brokers, newUris);
            }
            return this._brokers;
          } else {
            return brokers;
          }
        }
      }
    )
  }

  /**
   * @param brokers
   * @param uris
   * @private
   */
  _getBrokersForUris(brokers, uris) {
    const newBrokers = [];
    for (let i = 0; i < brokers.length; i++) {
      const newUriParts = uris[i].split(':');
      newBrokers.push({...brokers[i], host: newUriParts[0], port: parseInt(newUriParts[1])});
    }
    return newBrokers;
  }

  /**
   * @returns {Promise<TopicData[]>}
   */
  async getTopics() {

    if (this._connectionStatus === ConnectionStatus.CONNECTED) {
      this._topics = await new Promise(async (resolve, reject) => {

        try {
          const topic_meta = await this.admin.fetchTopicMetadata();

          console.log(topic_meta);

          resolve(topic_meta.topics.map((topic) => {

            return {
              name      : topic.name,
              partitions: topic.partitions.length
            }
          }));
        } catch (e) {
          reject(e);
        }
      });
    }

    return this._topics;
  }

  async getTopicOffsetHigh(topic) {
    const offsets = await this.admin.fetchTopicOffsets(topic);
    if (offsets.length > 0) {
      return parseInt(offsets[0].high);
    }
  }

  async getTopicOffsetLow(topic) {
    const offsets = await this.admin.fetchTopicOffsets(topic);
    if (offsets.length > 0) {
      return parseInt(offsets[0].low);
    }
  }

  async ping() {
    return new Promise((resolve, reject) => {

      if (!this.consumerClient) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
  }

  async disconnect() {

    this._disconnectRequested = true;
    clearInterval(this._connectionStatusIntervalId);

    await this._doDisconnect();

    this._connectionStatus = ConnectionStatus.DISCONNECTED;

    this.topicAges = {};
    this.messages  = {};
    this._topics   = [];
  }

  async _doDisconnect() {
    try {
      const consumerDisconnectPromises = [];
      const consumers                  = this.consumers;
      for (const c in consumers) {
        if (consumers[c]) {
          await consumers[c].disconnect();
        }
      }
      await new Promise(resolve => {
        setTimeout(resolve, 5000)
      });
      await Promise.all(consumerDisconnectPromises);

      const promises = [];
      if (this.admin) {
        promises.push(this.admin.disconnect());
      }
      if (this.producer) {
        promises.push(this.producer.disconnect());
      }

      this.consumers = {};

      let didComplete = false;
      await new Promise((resolve, reject) => {
        Promise.all(promises)
               .then(() => {
                 if (!didComplete) {
                   didComplete = true;
                   resolve();
                 }
               })
               .catch((e) => {
                 if (!didComplete) {
                   didComplete = true;
                   reject(e);
                 }
               })
        setTimeout(() => {
          if (!didComplete) {
            didComplete = true;
            reject(new Error("Timed out"));
          }
        }, 5000);
      });

    } catch (e) {
      if (e.message === "Timed out") {
        console.warn('Disconnect timed out');
      } else {
        console.error('Disconnect failed', e);
      }
    }
    this._connectionConfig = null;
  }

  oldestTopic() {
    let oldest     = null;
    let oldest_age = Infinity;
    for (const topic in this.topicAges) {
      if (this.messages.hasOwnProperty(topic) && this.topicAges[topic] < oldest_age && this.messages[topic].length) {
        oldest_age = this.topicAges[topic];
        oldest     = topic;
      }
    }
    return oldest;
  }

  getCacheUsed() {
    let cache_used = 0;
    console.log("STREAM CONTROLLER MESSAGES", this.messages);
    for (const prop in this.messages) {
      this.messages[prop].forEach((v) => {
        cache_used += jsonSize(v);
      });
    }
    this.cacheUsedSize = cache_used;
    return cache_used;
  }

  takeOutTheTrash() {
    let memory_size  = this.getCacheUsed();
    let oldest_topic = this.oldestTopic();
    while (memory_size > this._settingsDriver.cacheSize + 128 && oldest_topic) {
      this.messages[oldest_topic].shift();
      oldest_topic = this.oldestTopic();
    }
  }

  getConnectionConfig() {
    return this._connectionConfig
  }

  getClusterBrokerUris() {
    return this.clusterBrokers.getUris();
  }

  async deleteTopic(topic) {
    await this.admin.deleteTopics({
                              topics : [topic],
                              timeout: 2000,
                            })
  }

  async createTopics(options) {
      const result = await this.admin.createTopics(options);
      // We have to reconnect the producer after creating a topic because the producer will register as the same connection that created the topic
      // If the producer is the same connection that created the topic then when it publishes messages, the consumers cannot receive them because
      // the consumers are also the same connection and messages produces on a connection will not be consumed on the same connection
      await this.producer.disconnect();
      await this.producer.connect();
      return result;
  }

}

module.exports = {KafkaDriver};