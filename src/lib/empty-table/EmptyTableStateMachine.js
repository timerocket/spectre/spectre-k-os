const {EmptyTableState} = require('./EmptyTableState');

class EmptyTableStateMachine {

    /**
     * @private
     * @type {EmptyTableState}
     */
    _awaitingState;

    /**
     * @private
     * @type {EmptyTableState}
     */
    _pausedState;

    /**
     * @private
     */
    _loadingTopicsState;

    /**
     * @private
     */
    _noTopicsState;

    /**
     * @private
     */
    _noMatchingTopicsState;

    /**
     * @private
     */
    _noMatchingMessagesState;

    /**
     * @private
     * @type {EmptyTableState}
     */
    _state;

    _stateMap;

    constructor() {
        this._awaitingState           = new EmptyTableState("Awaiting Messages...", EmptyTableState.AWAITING, true);
        this._pausedState             = new EmptyTableState("Paused", EmptyTableState.PAUSED, false);
        this._loadingTopicsState      = new EmptyTableState("Loading Topics...", EmptyTableState.LOADING_TOPICS, true);
        this._noTopicsState           = new EmptyTableState("No topics", EmptyTableState.NO_TOPICS, false);
        this._noMatchingTopicsState   = new EmptyTableState("No matching topics", EmptyTableState.NO_MATCHING_TOPICS, false);
        this._noMatchingMessagesState = new EmptyTableState("No matching messages", EmptyTableState.NO_MATCHING_MESSAGES, false);
        this._state                   = this._awaitingState;
        this._stateMap = {
            [EmptyTableState.PAUSED]              : this._pausedState,
            [EmptyTableState.AWAITING]            : this._awaitingState,
            [EmptyTableState.LOADING_TOPICS]      : this._loadingTopicsState,
            [EmptyTableState.NO_TOPICS]           : this._noTopicsState,
            [EmptyTableState.NO_MATCHING_TOPICS]  : this._noMatchingTopicsState,
            [EmptyTableState.NO_MATCHING_MESSAGES]: this._noMatchingMessagesState,
        }
    }

    getNoTopicsState() {
        return this._noTopicsState;
    }

    getLoadingTopicsState() {
        return this._loadingTopicsState;
    }

    /**
     * @param {"PAUSED" | "AWAITING" | "LOADING_TOPICS" | "NO_TOPICS" | "NO_MATCHING_TOPICS" } state
     */
    setStateType(state) {
        this._state = this._stateMap[state];
    }

    setStateForStateType(state, type) {
        this._stateMap[type] = state;
    }

    /**
     * @returns {EmptyTableState}
     */
    get state() {
        return this._state;
    }
}

module.exports = {EmptyTableStateMachine}