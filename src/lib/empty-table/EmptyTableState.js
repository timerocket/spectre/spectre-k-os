
class EmptyTableState {

    static get PAUSED() { return "PAUSED" }
    static get AWAITING() { return "AWAITING"}
    static get LOADING_TOPICS() { return "LOADING_TOPICS"; }
    static get NO_TOPICS() { return "NO_TOPICS"; }
    static get NO_MATCHING_MESSAGES() { return "NO_MATCHING_MESSAGES"; }
    static get NO_MATCHING_TOPICS() { return "NO_MATCHING_TOPICS"; }

    /**
     * @private
     */
    _message;

    /**
     * @private
     */
    _type;

    /**
     * @private
     * @type {boolean}
     */
    _inProgress;

    /**
     * @param {string} message
     * @param {"PAUSED" | "AWAITING" | "LOADING_TOPICS" | "NO_TOPICS" | "NO_MATCHING_TOPICS" } type
     * @param {boolean} inProgress
     */
    constructor(message, type, inProgress) {
        this._message = message;
        this._type = type;
        this._inProgress = inProgress;
    }

    get message() {
        return this._message;
    }

    get type() {
        return this._type;
    }

    /**
     * @returns {boolean}
     */
    get inProgress() {
        return this._inProgress;
    }
}

module.exports = { EmptyTableState }