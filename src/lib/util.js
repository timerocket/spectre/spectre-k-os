
const bytes = (s) => {
    return s.length;
}

const jsonSize = (s) => {
    return bytes(JSON.stringify(s));
}

const getFormattedDate = (date) => {
    if(!date) {
        return '';
    }
    let year = date.getFullYear();

    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}

module.exports = {jsonSize, getFormattedDate}