// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process unless
// nodeIntegration is set to true in webPreferences.
// Use preload.js to selectively enable features
// needed in the renderer process.
const path        = require('path');
const fs          = require('fs');
const envFilePath = path.join(process.resourcesPath, '.env');
if (fs.existsSync(envFilePath)) {
    require('dotenv').config({path: envFilePath});
    console.log(`RUNTIME=${process.env.RUNTIME}`);
} else {
    console.log(`envFilePath: ${envFilePath} does not exist`)
}

if(process.env.RUNTIME === 'win'){
    require('win-ca');
}

const Router                      = require("./lib/Router");
const ConnectController           = require("./lib/module/ConnectController");
const TopicViewController         = require("./lib/module/TopicViewController");
const TopicStreamController       = require("./lib/module/TopicStreamController");
const {ConnectionDriver}          = require("./lib/connection/ConnectionDriver");
const {EventSource}               = require('./lib/EventSource');
const LicenseKey                  = require("./lib/LicenseKey");
const Modal                       = require("./lib/Modal");
const filePath                    = require("./lib/FilePath");
const SettingsController          = require("./lib/module/SettingsController");
const {ConnectionStatus}          = require("./lib/connection/ConnectionStatus");
const {ConnectionStatusIndicator} = require("./lib/connection/ConnectionStatusIndicator");
const {LicenseCheckStatus}        = require('./lib/LicenseCheckStatus');
const {getFormattedDate}          = require('./lib/util');
const {LicenseCheckResult}        = require('./lib/LicenseCheckResult');

const app = new Router();

const connect_controller      = new ConnectController(app);
const topic_view_controller   = new TopicViewController(app);
const topic_stream_controller = new TopicStreamController(app);
const settings_controller     = new SettingsController(app);

const connection_status_indicator = new ConnectionStatusIndicator(app, app.connection);

connect_controller.subscribe("connect", function () {
    app.route(topic_view_controller);
});

connect_controller.subscribe("check-result-spinner", function () {
    document.getElementById("connection_status").innerHTML = `<i class="fas fa-fw fa-spin fa-spinner"></i>`;
});

connect_controller.subscribe("check-result-bad", function (data) {
    document.getElementById("connection_status").innerHTML = `<i style="color:#e25353;" class="fas fa-fw fa-exclamation-circle"></i>${data ? (`<span class="tooltip">${data}</span>`) : ""}`;
});

connect_controller.subscribe("check-result-good", function () {
    document.getElementById("connection_status").innerHTML = `<i style="color:#8ae264;" class="fas fa-fw fa-check-circle"></i>`;
});

connect_controller.subscribe("check-result-reset", function () {
    document.getElementById("connection_status").innerHTML = `<i class="fas fa-fw fa-ellipsis-h"></i>`;
});

connect_controller.subscribe("uri-reset", function () {
    const uri = document.getElementById("uri");
    if (uri) {
        uri.style.backgroundColor = `#60728b`;
    }
});

connect_controller.subscribe("uri-bad", function () {
    const uri = document.getElementById("uri");
    if (uri) {
        uri.style.backgroundColor = `#e25353`;
    }
});

topic_view_controller.subscribe("focus", () => {
    connection_status_indicator.useConnectionStatus();
})

topic_stream_controller.subscribe("focus", () => {
    connection_status_indicator.useConnectionStatus();
})

topic_view_controller.subscribe("select-topic", function (topic) {
    topic_stream_controller.setTopic(topic);
    app.route(topic_stream_controller);
});

async function disconnect() {
    app.connection.disconnect();
    await app.route(connect_controller, EventSource.DISCONNECT);
}


topic_view_controller.subscribe("disconnect", disconnect);
app.subscribe("disconnect", disconnect);

let return_controller = connect_controller;
app.subscribe("settings", (c) => {
    return_controller = c;
    app.route(settings_controller);
})

app.subscribe("settings-return", (c) => {
    app.route(return_controller);
})

app.subscribe("message-new", (c) => {
    settings_controller.messages_total++;
})

app.subscribe("messages-clear", (c) => {
    settings_controller.messages_total -= c;
})

connect_controller.subscribe("refresh", () => {
    app.route(connect_controller, EventSource.REFRESH);
})

topic_stream_controller.subscribe("back", function () {
    app.route(topic_view_controller);
});

app.subscribe("settings-garbage-collect", function () {
    app.connection.getKafkaDriver().takeOutTheTrash();
});

// Open all links in external browser
let shell = require('electron').shell;
document.addEventListener('click', function (event) {
    if (event.target.tagName === 'A' && event.target.href.startsWith('http')) {
        event.preventDefault()
        shell.openExternal(event.target.href)
    }
})

const licenseKey = new LicenseKey();
let lastStatus;

function getLicenseKeyFailureMessage(licenseCheckResult) {
    let lastVerifiedMessage = '';
    if (licenseCheckResult.lastVerified) {
        lastVerifiedMessage = ` Last verified on ${getFormattedDate(licenseCheckResult.lastVerified)}.`;
    }
    if (licenseCheckResult.status === LicenseCheckStatus.UNABLE_TO_VERIFY) {
        lastVerifiedMessage += ` Error: ${licenseCheckResult.errorMessage}`;
    }
    const failureMessageMap = {
        [LicenseCheckStatus.CANCELLED]          : `Your subscription was cancelled on ${getFormattedDate(licenseCheckResult.cancelledAt)}`,
        [LicenseCheckStatus.INVALID_LICENSE_KEY]: "Invalid license key",
        [LicenseCheckStatus.PAYMENT_FAILED]     : `License key renewal failed on ${getFormattedDate(licenseCheckResult.failedAt)}`,
        [LicenseCheckStatus.TOO_MANY_USES]      : `License key used too many times. Uses: ${getFormattedDate(licenseCheckResult.uses)}`,
        [LicenseCheckStatus.UNABLE_TO_VERIFY]   : `Unable to verify license. Are you connected to the internet?${lastVerifiedMessage}`,
    }
    return failureMessageMap[licenseCheckResult.status];
}

/**
 * @param {LicenseCheckResult} licenseCheckResult
 */
function launchLicenseKeyModal(licenseCheckResult) {
    document.getElementById('initial-loading').innerHTML = '';
    let initialErrorMessage                              = getLicenseKeyFailureMessage(licenseCheckResult);
    const modal            = new Modal(ejs.render(fs.readFileSync(filePath("../view/license-key-modal.ejs")).toString(), {
        licenseKey  : licenseKey.user_key,
        errorMessage: initialErrorMessage
    }, null));

    const activate_element = document.getElementById('activate');
    const license_element  = document.getElementById('license-key');

    license_element.onclick = () => {
        license_element.style.backgroundColor = `#60728b`;
    }

    activate_element.onclick = async () => {
        let _key = license_element.value;
        if (_key.split("-").length !== 4 || _key.length !== ((8 * 4) + 3)) {
            license_element.style.backgroundColor = `#e25353`;
            return;
        }
        document.getElementById("license-key-form").style.display = "none";
        document.getElementById("license-key-status").innerHTML   = `<h4><i class="fas fa-2x fa-spin fa-circle-notch"></i><br/><br/>Authenticating License...</h4>`;
        const check                                               = await licenseKey.checkAuthorized(_key);

        if (check.status === LicenseCheckStatus.ACTIVE) {
            if ([LicenseCheckStatus.INVALID_LICENSE_KEY, LicenseCheckStatus.NO_LICENSE_KEY].includes(lastStatus)) {
                await licenseKey.consumeKey(_key);
                modal.close();
            }
            await app.route(connect_controller, EventSource.STARTUP);
        } else {
            const message                                                  = getLicenseKeyFailureMessage(check);
            document.getElementById("license-key-form").style.display      = "block";
            document.getElementById("license-key-error-message").innerHTML = message;
            document.getElementById("license-key-status").innerHTML        = "";
        }
        lastStatus = check.status;
    }
}

const startup = async () => {
    const licenseCheckResult = await licenseKey.isAuthorized();
    lastStatus               = licenseCheckResult.status
    if (![LicenseCheckStatus.ACTIVE, LicenseCheckStatus.SKIPPED_VERIFICATION].includes(licenseCheckResult.status)) {
        launchLicenseKeyModal(licenseCheckResult);
    } else {
        await app.route(connect_controller, EventSource.STARTUP);
    }
}

app.route(connect_controller, EventSource.STARTUP);

startup().then();

