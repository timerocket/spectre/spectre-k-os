const {Kafka} = require("kafkajs");
var faker = require('faker');

let connection_object = {
    clientId: 'my-producer-app',
    brokers: ['localhost:9092']
}

function generateMessage(){
    return {
        key:'user',
        value:JSON.stringify({ name:faker.name.findName(),
            email:faker.internet.email(),
            text:faker.lorem.words()})
    }
}

async function produce() {

    let consumerClient = new Kafka(connection_object);

    let producerClient = new Kafka(connection_object);
    let producer       = producerClient.producer();

    await producer.connect();

    let admin = producerClient.admin();
    await admin.connect();

    console.log('CREATED ADMIN');
    console.log(await admin.describeCluster());

    setInterval(()=>{
        producer.send({
            topic: 'service.consumer.message',
            messages: [generateMessage()],
        })
    }, 35);

}

produce();