# Spectre Kafka (Apache Kafka UI & Editor)

Have you ever been working in dev environment and wanted to view Kafka topics and messages without all of the hassle?

## Out of all the terrible Kafka Desktop Client GUIs, this one's the best

_Please use this in production._

## Releasing

### Mac App Store

Use the `.pkg` file in `./release/mas`. Do not use the file in `./release`